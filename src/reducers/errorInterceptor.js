import axios from 'axios';
import findMyIdentity from './../lib/findMyIdentity';
import logger from './../lib/logger';
import { deleteSessionCookie } from './cookies';

const interceptError = (error, refreshToken, updateBearerToken) => {
    logger.warn('interceptError(): ' + error);
    const originalRequest = error.config;

    if (error?.response?.status === 401 && !originalRequest._retry) {
        originalRequest._retry = true;

        logger.debug('Refresh token: ...' + refreshToken.substring(refreshToken.length - 10));

        const promise = new Promise((resolutionFunc, rejectionFunc) => {
            findMyIdentity.refreshSession(refreshToken, function (err, res) {
                if (err) rejectionFunc(err);
                else resolutionFunc(res);
            });
        });

        promise
            .then((bearerToken) => {
                logger.debug('Refresh succeeded');
                updateBearerToken(bearerToken);
                return axios(originalRequest);
            })
            .catch((err) => {
                logger.error(err.message);
                return Promise.reject(error);
            });
    } else if (error?.response?.status === 401) {
        findMyIdentity.logout();
        deleteSessionCookie();
        return Promise.reject(error);
    } else return Promise.reject(error);
};

const errorInterceptor = (error, refreshToken, updateBearerToken) => {
    return interceptError(error, refreshToken, updateBearerToken);
};

export default errorInterceptor;

import Cookies from 'universal-cookie';

const cookies = new Cookies();

export const getSessionCookie = () => cookies.get('token', { path: '/' });

export const setSessionCookie = (session) => cookies.set('token', JSON.stringify(session), { path: '/' });

export const deleteSessionCookie = () => cookies.remove('token', { path: '/' });

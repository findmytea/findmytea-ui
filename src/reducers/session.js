import axios from 'axios';
import { getSessionCookie, setSessionCookie, deleteSessionCookie } from './cookies';
import { CLEAR_SESSION, SET_SESSION } from '../constants/actionTypes';
import errorInterceptor from './errorInterceptor';

const initialState = () => {
    const cookie = getSessionCookie();

    if (cookie) {
        updateSession(cookie);
        return cookie;
    } else {
        return { isLoggedIn: false };
    }
};

const session = (state = initialState(), action) => {
    switch (action.type) {
        case SET_SESSION:
            return onSetSession(action);

        case CLEAR_SESSION:
            return onClearSession();

        default:
            return onGetState(state);
    }
};

export default session;

const onSetSession = (action) => {
    const state = Object.assign({}, action.session, { isLoggedIn: true });
    updateSession(state);
    return state;
};

const updateSession = (session) => {
    setSessionCookie(session);
    setBearerToken(session);

    axios.interceptors.response.use(
        (response) => {
            return response;
        },
        (e) => {
            const refreshToken = session.credentials.idToken;
            return errorInterceptor(e, refreshToken, function (token) {
                session.credentials.bearerToken = token;
                updateSession(session);
            });
        }
    );
};

const onClearSession = () => {
    deleteSessionCookie();
    axios.defaults.headers.common['Authorization'] = null;
    return initialState();
};

const onGetState = (state) => {
    return state;
};

const setBearerToken = (session) => {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + session?.credentials?.bearerToken;
};

export function mapStateToProps(state) {
    return { session: state.session };
}

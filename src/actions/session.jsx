import { CLEAR_SESSION, SET_SESSION } from '../constants/actionTypes';
import findMyIdentity from '../lib/findMyIdentity';
import { configureLoggerUser } from '../lib/logger';

export const clearSession = () => ({
    type: CLEAR_SESSION,
});

export function initSessionFromCallbackURI(callbackHref) {
    return function (dispatch) {
        return findMyIdentity
            .parseWebResponse(callbackHref) // parse the callback URL
            .then((response) => {
                const session = findMyIdentity.getSession(response.data);
                configureLoggerUser(session?.user?.userName);
                dispatch(setSession(session));
            });
    };
}

export const setSession = (session) => ({
    type: SET_SESSION,
    session,
});

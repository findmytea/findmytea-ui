import React, { useState } from 'react';
import { connect } from 'react-redux';
import { ROUTES } from './../config';
import { Redirect } from 'react-router-dom';
import Session from '../models/Session';
import Messages from '../components/Messages';
import LocationForm from '../components/LocationForm';
import LocationDetail from '../models/LocationDetail';
import { createLocation } from '../services/api/LocationApi';
import { useHistory } from 'react-router-dom';
import { mapStateToProps } from '../reducers/session';
import { Container } from '@material-ui/core';

interface CreateLocationProps {
    session: Session;
}

const CreateLocation = (props: CreateLocationProps) => {
    const history = useHistory();

    const [location] = useState(new LocationDetail());
    const [error, setError] = useState('');
    const [info, setInfo] = useState('');
    const [success, setSuccess] = useState('');
    const [loading, setLoading] = useState(false);

    if (!props.session.isLoggedIn) {
        return <Redirect to={ROUTES.HOME} />;
    }

    const create = async (location: LocationDetail) => {
        return new Promise(async (success, failure) => {
            setInfo('Saving');
            setError('');
            setLoading(true);

            try {
                const response = await createLocation(location);
                setInfo('');
                setSuccess('Saved');
                success(response);
                setTimeout(function () {
                    history.push(ROUTES.LOCATIONS);
                }, 500);
            } catch (error) {
                setInfo('');
                setError(error.message);
                failure(error);
            } finally {
                setLoading(false);
            }
        });
    };

    return (
        <Container>
            <Messages error={error} info={info} success={success}></Messages>
            <LocationForm location={location} onSubmit={create} disabled={loading} />
        </Container>
    );
};

export default connect(mapStateToProps)(CreateLocation);

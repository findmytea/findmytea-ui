import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Session from './../models/Session';
import { ROUTES } from './../config';
import { Link } from 'react-router-dom';
import { getLocations, deleteLocation } from '../services/api/LocationApi';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Storefront from '@material-ui/icons/Storefront';
import LocalCafe from '@material-ui/icons/LocalCafe';
import Location from '../models/Location';

import Messages from '../components/Messages';
import { Snackbar, Typography, Container } from '@material-ui/core';
import BrandGallery from '../components/BrandGallery';
import { mapStateToProps } from '../reducers/session';
import Img from './../components/Image';

interface LocationsProps {
    session: Session;
}

const Locations = (props: LocationsProps) => {
    const classes = useStyles();

    const [error, setError] = useState('');
    const [info, setInfo] = useState('');
    const [success] = useState('');

    const [rows, setRows] = useState(Array<Location>());

    const loadLocations = () => {
        setInfo('Loading');
        setError('');
        getLocations()
            .then((response) => {
                setRows(response.locations);
                setInfo('');
            })
            .catch((error: any) => {
                setInfo('');
                setError(error.Message);
            });
    };

    useEffect(() => {
        loadLocations();
    }, []);

    if (!props.session.isLoggedIn) {
        return <Redirect to={ROUTES.HOME} />;
    }

    const onDelete = (id: string) => {
        setInfo('Deleting');
        setError('');
        deleteLocation(id)
            .then((response) => {
                loadLocations();
                setInfo('');
            })
            .catch((error: any) => {
                setInfo('');
                setError(error.message);
            });
    };

    return (
        <Container>
            <Messages error={error} info={info} success={success}></Messages>
            <Snackbar open={true} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                <Link color="inherit" to={ROUTES.CREATE_LOCATION}>
                    <Fab color="primary" aria-label="add">
                        <AddIcon />
                    </Fab>
                </Link>
            </Snackbar>
            <h1 className={classes.title}>Locations</h1>

            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell></TableCell>
                            <TableCell>Name</TableCell>
                            <TableCell>Address</TableCell>
                            <TableCell>Cafe</TableCell>
                            <TableCell>Retailer</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map((row) => (
                            <TableRow key={row.id}>
                                <TableCell>
                                    <Img src={row?.image?.uri} alt={row.name} className={classes.image} />
                                </TableCell>
                                <TableCell>
                                    <Typography>
                                        <span title={row.id}>{row.name}</span>
                                    </Typography>
                                    <BrandGallery brands={row?.brands} imageHeight={25} />
                                </TableCell>
                                <TableCell>
                                    <ul className={classes.address}>
                                        <li>{row.addressLine1}</li>
                                        <li>{row.addressLine2}</li>
                                        <li>{row.town}</li>
                                        <li>{row.county}</li>
                                        <li>{row.postcode}</li>
                                    </ul>
                                </TableCell>
                                <TableCell>
                                    <div hidden={!row.cafe}>
                                        <Fab color="primary" size="small" aria-label="Cafe" className={classes.icon}>
                                            <LocalCafe />
                                        </Fab>
                                    </div>
                                </TableCell>
                                <TableCell>
                                    <div hidden={!row.retailer}>
                                        <Fab color="primary" size="small" aria-label="retailer" className={classes.icon}>
                                            <Storefront />
                                        </Fab>
                                    </div>
                                </TableCell>
                                <TableCell>
                                    <Link color="inherit" to={ROUTES.EDIT_LOCATION.replace(':id', row.id)}>
                                        <Fab color="secondary" size="small" aria-label="edit" className={classes.icon}>
                                            <EditIcon />
                                        </Fab>
                                    </Link>
                                    <Fab color="secondary" size="small" aria-label="delete" className={classes.icon}>
                                        <DeleteIcon
                                            onClick={() => {
                                                onDelete(row.id);
                                            }}
                                        />
                                    </Fab>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Container>
    );
};

export default connect(mapStateToProps)(Locations);

const useStyles = makeStyles({
    title: {
        flexGrow: 1,
    },
    table: {
        minWidth: 650,
    },
    image: {
        height: 50,
    },
    icon: {
        marginLeft: 5,
        marginRight: 5,
    },
    address: {
        listStyleType: 'none',
    },
});

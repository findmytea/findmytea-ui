import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link, useHistory } from 'react-router-dom';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Session from './../models/Session';
import { ROUTES } from './../config';
import Brand from '../models/Brand';
import { getBrands, deleteBrand, importBrandFromTwitter, importBrandFromCsv } from '../services/api/BrandApi';
import Messages from '../components/Messages';
import { Snackbar, Container } from '@material-ui/core';
import { mapStateToProps } from '../reducers/session';
import Img from './../components/Image';
import ImportMenu from '../components/Import/ImportMenu';
import ImportTwitterDialog from '../components/Import/ImportTwitterDialog';
import { SocialItem } from '../models/SocialItem';
import ImportCsvDialog from '../components/Import/ImportCsvDialog';

interface BrandsProps {
    session: Session;
}

const Brands = (props: BrandsProps) => {
    const classes = useStyles();
    const history = useHistory();

    const [rows, setRows] = useState(Array<Brand>());
    const [processing, setProcessing] = useState(false);

    const [importTwitterDialogOpen, setImportTwitterDialogOpen] = useState(false);
    const [importCsvDialogOpen, setImportCsvDialogOpen] = useState(false);

    const [error, setError] = useState('');
    const [info, setInfo] = useState('');
    const [success] = useState('');

    const onClickImportTwitter = () => setImportTwitterDialogOpen(true);

    const onClickImportCsv = () => setImportCsvDialogOpen(true);

    const onImportTwitter = async (social: SocialItem) => {
        setImportTwitterDialogOpen(false);

        setProcessing(true);
        setInfo(`Importing ${social.name}`);
        setError('');

        try {
            const response = await importBrandFromTwitter(+social.id);
            setInfo('');
            history.push(ROUTES.EDIT_BRAND.replace(':id', response.id));
        } catch (error) {
            setInfo('');
            setError(error.message);
        } finally {
            setProcessing(false);
        }
    };

    const onImportCsv = async (file: any) => {
        setImportTwitterDialogOpen(false);

        setProcessing(true);
        setInfo(`Importing `);
        setError('');

        try {
            await importBrandFromCsv(file);
            setInfo('');
            loadBrands();
        } catch (error) {
            setInfo('');
            setError(error.message);
        } finally {
            setProcessing(false);
        }
    };

    const loadBrands = async () => {
        setInfo('Loading');
        setError('');
        setProcessing(true);
        try {
            const response = await getBrands();
            setRows(response.brands);
            setInfo('');
        } catch (error) {
            setInfo('');
            setError(error.message);
        } finally {
            setProcessing(false);
        }
    };

    useEffect(() => {
        loadBrands();
    }, [setInfo, setError]);

    if (!props.session.isLoggedIn) {
        return <Redirect to={ROUTES.HOME} />;
    }

    const onDelete = async (id: string) => {
        setInfo('Deleting');
        setError('');
        setProcessing(true);

        try {
            await deleteBrand(id);
            loadBrands();
            setInfo('');
        } catch (error) {
            setInfo('');
            setError(error.message);
        } finally {
            setProcessing(false);
        }
    };

    return (
        <>
            <ImportTwitterDialog open={importTwitterDialogOpen} onImport={onImportTwitter} onClose={() => setImportTwitterDialogOpen(false)} />
            <ImportCsvDialog open={importCsvDialogOpen} onImport={onImportCsv} onClose={() => setImportCsvDialogOpen(false)} />

            <Container>
                <Snackbar open={true} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                    <Link color="inherit" to={ROUTES.CREATE_BRAND}>
                        <Fab color="primary" aria-label="add" disabled={processing}>
                            <AddIcon />
                        </Fab>
                    </Link>
                </Snackbar>

                <Messages error={error} info={info} success={success}></Messages>
                <h1 className={classes.title}>Tea Brands</h1>
                <ImportMenu onImportTwitter={onClickImportTwitter} onImportCsv={onClickImportCsv} disabled={processing} />

                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell></TableCell>
                                <TableCell>Name</TableCell>
                                <TableCell>Description</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map((row) => (
                                <TableRow key={row.id}>
                                    <TableCell>
                                        <Link color="inherit" to={ROUTES.EDIT_BRAND.replace(':id', row.id)}>
                                            <Img src={row.image?.uri} alt={row.name} className={classes.image} />
                                        </Link>
                                    </TableCell>
                                    <TableCell>
                                        <span title={row.id}>{row.name}</span>
                                    </TableCell>
                                    <TableCell>{row.description}</TableCell>
                                    <TableCell style={{ minWidth: 200 }}>
                                        <Link color="inherit" to={ROUTES.EDIT_BRAND.replace(':id', row.id)}>
                                            <Fab color="secondary" size="small" aria-label="edit" disabled={processing}>
                                                <EditIcon className="classes.icon" />
                                            </Fab>
                                        </Link>
                                        <Fab color="secondary" size="small" aria-label="delete" disabled={processing}>
                                            <DeleteIcon
                                                className="classes.icon"
                                                onClick={() => {
                                                    onDelete(row.id);
                                                }}
                                            />
                                        </Fab>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Container>
        </>
    );
};

export default connect(mapStateToProps)(Brands);

const useStyles = makeStyles({
    title: {
        flexGrow: 1,
    },
    table: {
        minWidth: 650,
    },
    image: {
        height: 50,
    },
    icon: {
        marginLeft: 5,
        marginRight: 5,
    },
});

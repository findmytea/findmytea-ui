import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { AxiosResponse } from 'axios';
import SearchLocation from '../components/SearchLocation';
import { search } from './../services/api/LocationSearch';
import Messages from '../components/Messages';
import SearchLocationResults from '../components/SearchLocationResults';
import LocationResponse from '../models/LocationResponse';
import BrandGallery from '../components/BrandGallery';
import Brand from '../models/Brand';
import { getBrands } from '../services/api/BrandApi';
import { Box, Container } from '@material-ui/core';
import logger from '../lib/logger';

export function Search() {
    const classes = useStyles();
    const [searching, setSearching] = useState(false);
    const [brands, setBrands] = useState(Array<Brand>());

    const [error, setError] = useState('');
    const [info, setInfo] = useState('');
    const [success, setSuccess] = useState('');
    const [results, setResults] = useState(new LocationResponse());

    const loadBrands = () => {
        getBrands()
            .then((response) => {
                setBrands(response.brands);
            })
            .catch((error: any) => {
                logger.warn(error.message);
            });
    };

    useEffect(() => {
        loadBrands();
    }, []);

    const resetMessages = () => {
        setError('');
        setInfo('');
        setSuccess('');
    };

    const onSearch = (searchTerm: string) => {
        setSearching(true);

        resetMessages();
        setInfo('Searching');

        search(searchTerm)
            .then((response: AxiosResponse<LocationResponse>) => {
                setResults(response.data.locations != null ? response.data : new LocationResponse());
                setSearching(false);
                resetMessages();
            })
            .catch((error) => {
                setSearching(false);
                resetMessages();
                setError(error.message);
            });
    };

    return (
        <Container>
            <Messages error={error} info={info} success={success}></Messages>
            <SearchLocation onSearch={onSearch} searching={searching}></SearchLocation>
            <SearchLocationResults results={results}></SearchLocationResults>
            <Box className={classes.brandsBox}>
                <BrandGallery brands={brands} imageHeight={50} />
            </Box>
        </Container>
    );
}

const useStyles = makeStyles(() => {
    return {
        brandsBox: {
            marginTop: '10px',
        },
    };
});

import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Redirect, useHistory } from 'react-router-dom';
import Session from '../models/Session';

import { mapStateToProps } from '../reducers/session';
import { Container } from '@material-ui/core';
import Messages from '../components/Messages';
import BrandForm from '../components/BrandForm/BrandForm';
import { ROUTES } from '../config';
import { createBrand } from '../services/api/BrandApi';
import BrandDetail from '../models/BrandDetail';

interface CreateBrandProps {
    session: Session;
}

const CreateBrand = (props: CreateBrandProps) => {
    const history = useHistory();

    const [brand] = useState(new BrandDetail());
    const [error, setError] = useState('');
    const [info, setInfo] = useState('');
    const [success, setSuccess] = useState('');
    const [loading, setLoading] = useState(false);

    if (!props.session.isLoggedIn) {
        return <Redirect to={ROUTES.HOME} />;
    }

    const create = async (brand: BrandDetail) => {
        return new Promise(async (success, failure) => {
            setInfo('Saving');
            setLoading(true);
            setError('');

            try {
                const response = await createBrand(brand);
                setInfo('');
                setSuccess('Saved');
                success(response);
                setTimeout(function () {
                    history.push(ROUTES.BRANDS);
                }, 500);
            } catch (error) {
                setInfo('');
                setError(error.message);
                failure(error);
            } finally {
                setLoading(false);
            }
        });
    };

    return (
        <Container>
            <Messages error={error} info={info} success={success}></Messages>
            <BrandForm brand={brand} onSubmit={create} disabled={loading} />
        </Container>
    );
};

export default connect(mapStateToProps)(CreateBrand);

import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { ROUTES } from './../config';
import { Redirect, useParams } from 'react-router-dom';
import Session from '../models/Session';
import { getLocation, updateLocation } from '../services/api/LocationApi';
import Messages from '../components/Messages';
import LocationDetail from '../models/LocationDetail';
import LocationForm from '../components/LocationForm';
import { mapStateToProps } from '../reducers/session';
import { Container } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

interface EditLocationProps {
    session: Session;
}

const EditLocation = (props: EditLocationProps) => {
    const history = useHistory();
    const { id } = useParams();

    const [location, setLocation] = useState(new LocationDetail());
    const [error, setError] = useState('');
    const [info, setInfo] = useState('');
    const [success, setSuccess] = useState('');
    const [loading, setLoading] = useState(true);

    const loadLocation = async (id: string) => {
        setInfo('Loading');
        setLoading(true);
        setError('');

        try {
            const response = await getLocation(id);
            setLocation(response);
            setInfo('');
        } catch (error) {
            setInfo('');
            setError(error.message);
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        if (id) loadLocation(id);
        else setError('Invalid Location Id.');
    }, [id]);

    if (!props.session.isLoggedIn) {
        return <Redirect to={ROUTES.HOME} />;
    }

    const update = async (location: LocationDetail) => {
        return new Promise(async (success, failure) => {
            setInfo('Saving');
            setLoading(true);
            setError('');
            try {
                const response = await updateLocation(location);
                setInfo('');
                setSuccess('Saved');
                success(response);
                setTimeout(function () {
                    history.push(ROUTES.LOCATIONS);
                }, 500);
            } catch (error) {
                setInfo('');
                setError(error.message);
                failure(error);
            } finally {
                setLoading(false);
            }
        });
    };

    return (
        <Container>
            <Messages error={error} info={info} success={success}></Messages>
            <LocationForm location={location} onSubmit={update} disabled={loading} />
        </Container>
    );
};

export default connect(mapStateToProps)(EditLocation);

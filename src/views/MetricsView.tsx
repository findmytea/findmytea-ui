import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Session from './../models/Session';
import { Redirect } from 'react-router';
import { ROUTES } from '../config';
import { mapStateToProps } from '../reducers/session';
import { connect } from 'react-redux';
import { Container } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { ArgumentAxis, ValueAxis, Chart } from '@devexpress/dx-react-chart-material-ui';
import { BarSeries, Stack } from '@devexpress/dx-react-chart';
import { SearchPerDay } from '../models/SearchPerDay';
import Messages from '../components/Messages';
import { getSearchesByDay } from '../services/api/MetricsApi';

interface MetricsProps {
    session: Session;
}

const Metrics = (props: MetricsProps) => {
    const classes = useStyles();
    const [searchesByDay, setSearchesByDay] = useState(Array<SearchPerDay>());

    const [error, setError] = useState('');
    const [info, setInfo] = useState('');
    const [success] = useState('');

    useEffect(() => {
        loadSearchesByDay();
    }, [setInfo, setError]);

    if (!props.session.isLoggedIn) {
        return <Redirect to={ROUTES.HOME} />;
    }

    const loadSearchesByDay = async () => {
        setInfo('Loading');
        setError('');
        try {
            const response = await getSearchesByDay();
            setSearchesByDay(response.searchesPerDay);
            setInfo('');
        } catch (error) {
            setInfo('');
            setError(error.message);
        } finally {
        }
    };

    return (
        <>
            <Messages error={error} info={info} success={success}></Messages>
            <Container>
                <h1 className={classes.title}>Searches/Day</h1>
                <Paper>
                    <Chart data={searchesByDay}>
                        <ArgumentAxis />
                        <ValueAxis />

                        <BarSeries valueField="numberOfSearches" argumentField="dateTime" name="Searches" />
                        <Stack />
                    </Chart>
                </Paper>
            </Container>
        </>
    );
};

export default connect(mapStateToProps)(Metrics);

const useStyles = makeStyles({
    title: {
        flexGrow: 1,
    },
});

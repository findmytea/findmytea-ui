import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Redirect, useParams, useHistory } from 'react-router-dom';
import Session from '../models/Session';
import Messages from '../components/Messages';
import { getBrand, updateBrand } from '../services/api/BrandApi';
import { ROUTES } from './../config';
import BrandForm from '../components/BrandForm';

import { mapStateToProps } from '../reducers/session';
import BrandDetail from '../models/BrandDetail';
import { Container } from '@material-ui/core';

interface EditBrandProps {
    session: Session;
}

const EditBrand = (props: EditBrandProps) => {
    const history = useHistory();
    const { id } = useParams();

    const [brand, setBrand] = useState(new BrandDetail());
    const [error, setError] = useState('');
    const [info, setInfo] = useState('');
    const [success, setSuccess] = useState('');
    const [loading, setLoading] = useState(true);

    const loadBrand = async (id: string) => {
        setInfo('Loading');
        setLoading(true);
        setError('');

        try {
            const response = await getBrand(id);
            setBrand(response);
            setInfo('');
        } catch (error) {
            setInfo('');
            setError(error.message);
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        if (id) loadBrand(id);
        else setError('Invalid Brand Id.');
    }, [id]);

    if (!props.session.isLoggedIn) {
        return <Redirect to={ROUTES.HOME} />;
    }

    const update = async (brand: BrandDetail) => {
        return new Promise(async (success, failure) => {
            setInfo('Saving');
            setError('');
            setLoading(true);

            try {
                const response = await updateBrand(brand);
                setInfo('');
                setSuccess('Saved');
                success(response);
                setTimeout(function () {
                    history.push(ROUTES.BRANDS);
                }, 500);
            } catch (error) {
                setInfo('');
                setError(error.message);
                failure(error);
            } finally {
                setLoading(false);
            }
        });
    };

    return (
        <Container>
            <Messages error={error} info={info} success={success}></Messages>
            <BrandForm brand={brand} onSubmit={update} disabled={loading} />
        </Container>
    );
};

export default connect(mapStateToProps)(EditBrand);

import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { ROUTES } from './../config';
import * as Routes from './routes';
import Authenticate from './routes/Authenticate';

export default function AppRoutes() {
    return (
        <Switch>
            <Route exact path="/callback">
                <Authenticate />
            </Route>
            <Route exact path="/">
                <Redirect to={ROUTES.SEARCH} />
            </Route>
            {/* <Switch /> is funny about rendering routes inside fragments in these nested components. 
        For some reason wrapping them all with this outer fragment them makes it work. 
        Do not remove! */}
            <>
                <Routes.Search />
                <Routes.Brand />
                <Routes.Location />
                <Routes.Metrics />
            </>
        </Switch>
    );
}

import React, { useEffect } from 'react';
import { Redirect, useLocation } from 'react-router-dom';
import { connect } from 'react-redux';
import { initSessionFromCallbackURI } from './../../actions/session';
import Session from '../../models/Session';
import { mapStateToProps } from '../../reducers/session';

function mapDispatchToProps(dispatch: any) {
    return {
        initSessionFromCallbackURI: (href: any) => dispatch(initSessionFromCallbackURI(href)),
    };
}

interface AuthenticateProps {
    initSessionFromCallbackURI: Function;
    session: Session;
}

const Authenticate = (props: AuthenticateProps) => {
    const location = useLocation();

    useEffect(() => {
        if (location.search || location.hash) {
            props.initSessionFromCallbackURI(window.location.href);
        }
    }, [location, props]);

    if (props.session.isLoggedIn) {
        return <Redirect to="/" />;
    }

    return null;
};

export default connect(mapStateToProps, mapDispatchToProps)(Authenticate);

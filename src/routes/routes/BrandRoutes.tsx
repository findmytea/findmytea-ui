import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { ROUTES } from '../../config';
import Brands from './../../views/BrandsView';
import CreateBrand from '../../views/CreateBrandView';
import EditBrand from './../../views/EditBrandView';

export function Brand() {
    return (
        <Switch>
            <Route exact path={ROUTES.BRANDS} render={(props) => <Brands />} />
            <Route exact path={ROUTES.CREATE_BRAND} render={(props) => <CreateBrand />} />
            <Route exact path={ROUTES.EDIT_BRAND} render={(props) => <EditBrand />} />
        </Switch>
    );
}

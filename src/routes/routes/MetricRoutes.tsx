import React from 'react';
import { Route } from 'react-router-dom';
import { ROUTES } from '../../config';
import MetricsView from './../../views/MetricsView';

export function Metrics() {
    return <Route exact path={ROUTES.METRICS} render={(props) => <MetricsView />} />;
}

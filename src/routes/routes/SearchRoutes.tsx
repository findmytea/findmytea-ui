import React from 'react';
import { Route } from 'react-router-dom';
import { ROUTES } from '../../config';
import * as View from './../../views/index';

export function Search() {
    return (
        <Route exact path={ROUTES.SEARCH} render={(props) => <View.Search />} />
    );
}

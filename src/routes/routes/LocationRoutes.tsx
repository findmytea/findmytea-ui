import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { ROUTES } from '../../config';
import Locations from './../../views/LocationsView';
import CreateLocation from './../../views/CreateLocationView';
import EditLocation from './../../views/EditLocationView';

export function Location() {
    return (
        <Switch>
            <Route
                exact
                path={ROUTES.LOCATIONS}
                render={(props) => <Locations />}
            />
            <Route
                exact
                path={ROUTES.CREATE_LOCATION}
                render={(props) => <CreateLocation />}
            />
            <Route
                exact
                path={ROUTES.EDIT_LOCATION}
                render={(props) => <EditLocation />}
            />
        </Switch>
    );
}

const ROUTES = {
    HOME: '/',
    SEARCH: '/search',
    BRANDS: '/brand',
    EDIT_BRAND: '/brand/:id',
    CREATE_BRAND: '/brand/new',

    LOCATIONS: '/location',
    CREATE_LOCATION: '/location/new',
    EDIT_LOCATION: '/location/:id',

    METRICS: '/metrics',
};

export { ROUTES };

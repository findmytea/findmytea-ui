import axios from 'axios';

axios.defaults.baseURL = process.env.REACT_APP_API_BASE + '/';

const ENDPOINTS = {
    LOACTION_SEARCH: 'LocationSearch',

    LOCATIONS: 'location',
    DELETE_LOCATION: 'location/:Id',
    LOCATION: 'location/:Id',
    CREATE_LOCATION: 'location',
    UPDATE_LOCATION: 'location/:Id',

    BRANDS: 'brand',
    BRAND: 'brand/:Id',
    DELETE_BRAND: 'brand/:Id',
    UPDATE_BRAND: 'brand/:Id',
    CREATE_BRAND: 'brand',
    TWITTER_ID_EXISTS: 'brand/check?twitterid=:Id',
    BRAND_NAME_EXISTS: 'brand/check?name=:name',
    IMPORT_BRAND_FROM_TWITTER: 'Brand/Twitter/',
    IMPORT_BRAND_FROM_CSV: 'Brand/Csv/',

    TWITTER_USER_SEARCH: 'twitter',
    FACEBOOK_PAGE_SEARCH: 'facebook',
    ADDRESS_LOOKUP: 'AddressLookup?searchTerm=:searchTerm',
    GEO_CODE: 'GeoCode?searchTerm=:Address',

    IMAGES: 'Image',
    IMAGE_UPLOAD: 'Image',

    METRICS_SEARCHES_BY_DAY: 'Metrics/SearchesPerDay',
};

const HEADERS = {
    headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
    },
};

export { ENDPOINTS as API };
export { HEADERS };

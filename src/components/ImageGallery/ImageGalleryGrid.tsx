import React from 'react';
import Image from '../../models/Image';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Grid, Card, CardActionArea, CardMedia } from '@material-ui/core';

export interface ImageGalleryGridProps {
    images: Array<Image>;
    onSelect(image: Image): void;
}

const ImageGalleryGrid = (props: ImageGalleryGridProps) => {
    const classes = useStyles();

    return (
        <Box>
            <Grid container spacing={3} className={classes.grid}>
                {props.images.map((image: Image, index: number) => {
                    return (
                        <Grid item key={index}>
                            <Card onClick={() => props.onSelect(image)}>
                                <CardActionArea>
                                    <CardMedia className={classes.image} alt={image.id} image={image.uri} component="img" />
                                </CardActionArea>
                            </Card>
                        </Grid>
                    );
                })}
            </Grid>
        </Box>
    );
};

export default ImageGalleryGrid;

const useStyles = makeStyles({
    grid: {
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 5,
        paddingRight: 5,
        width: '100%',
        maxHeight: '650px',
    },
    image: {
        maxWidth: '200px',
        height: '200px',
    },
});

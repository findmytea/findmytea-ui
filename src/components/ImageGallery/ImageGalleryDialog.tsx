import React, { useState } from 'react';
import { Button, Dialog, DialogTitle, Box, makeStyles } from '@material-ui/core';
import Image from '../../models/Image';
import { uploadImage } from '../../services/api/ImagesApi';
import ImageGalleryGrid from './ImageGalleryGrid';
import ImageGalleryUpload from './ImageGalleryUpload';

interface ImageGalleryDialogProp {
    images: Array<Image>;
    onSelect(image: Image): void;
    onUploaded(image: Image): void;
    disabled: boolean;
}

const ImageGalleryDialog = (props: ImageGalleryDialogProp) => {
    const classes = useStyles();
    const [open, setOpen] = useState(false);

    const onOpen = () => {
        setOpen(true);
    };

    const onClose = () => {
        setOpen(false);
    };

    const onSelect = (image: Image) => {
        props.onSelect(image);
        setOpen(false);
    };

    return (
        <>
            <Button variant="contained" color="primary" onClick={onOpen} disabled={props.disabled}>
                Select Image
            </Button>

            <Dialog aria-labelledby="simple-dialog-title" open={open}>
                <DialogTitle id="simple-dialog-title">
                    <span className={classes.dialogTitle}>Gallery</span>
                    <Button onClick={onClose}>Close</Button>
                </DialogTitle>
                <Box className={classes.dialogBody}>
                    <Box>
                        <ImageGalleryGrid images={props.images} onSelect={onSelect} />
                    </Box>

                    <Box>
                        <ImageGalleryUpload onUpload={uploadImage} onUploaded={props.onUploaded} />
                    </Box>
                </Box>
            </Dialog>
        </>
    );
};

export default ImageGalleryDialog;

const useStyles = makeStyles((theme) => ({
    dialogBody: {
        maxHeight: 650,
        maxWidth: 700,
    },
    dialogTitle: {
        marginRight: theme.spacing(2),
    },
}));

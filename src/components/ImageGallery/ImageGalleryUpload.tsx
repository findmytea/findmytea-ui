import React, { useState } from 'react';
import Dropzone from 'react-dropzone';
import { makeStyles, Box, Paper, CircularProgress } from '@material-ui/core';
import FileIcon from '@material-ui/icons/InsertDriveFile';
import CheckIcon from '@material-ui/icons/Check';
import ErrorIcon from '@material-ui/icons/Error';
import { green, red } from '@material-ui/core/colors';
import Image from '../../models/Image';

export interface ImageGalleryUploadProps {
    onUpload(file: any): any;
    onUploaded(image: Image): void;
}

const ImageGalleryUpload = (props: ImageGalleryUploadProps) => {
    const classes = useStyles();
    const [file, setFile] = useState();
    const [pending, setPending] = useState(false);
    const [fileDetails, setFileDetails] = useState('');
    const [error, setError] = useState(false);
    const [success, setSuccess] = useState(false);

    const onDrop = (acceptedFiles: any) => {
        setFile(acceptedFiles[0]);

        setSuccess(false);
        setError(false);
        setFileDetails(`Uploading ${acceptedFiles[0].name} (${bytes(acceptedFiles[0].size)})`);
        setPending(true);

        props
            .onUpload(acceptedFiles[0])
            .then((image: Image) => {
                props.onUploaded(image);
                setPending(false);
                setSuccess(true);
                setFile(undefined);
            })
            .catch((e: any) => {
                setPending(false);
                setError(e.message);
            });
    };

    return (
        <Box className={classes.root}>
            <Dropzone onDrop={onDrop} multiple={false} accept="image/jpeg, image/png">
                {({ getRootProps, getInputProps }) => (
                    <section>
                        <div {...getRootProps()}>
                            <input {...getInputProps()} />
                            <Paper className={classes.dropzone} elevation={0}>
                                <div className={classes.dropzoneInner}>
                                    {!file && !pending && !error && !success && (
                                        <>
                                            <FileIcon className={classes.dropIcon} />
                                            <p>Drop an image file here, or click to select files</p>
                                        </>
                                    )}
                                    {pending && (
                                        <>
                                            <CircularProgress size={40} className={classes.dropIcon} />
                                        </>
                                    )}
                                    {success && (
                                        <>
                                            <CheckIcon className={`${classes.dropIcon} ${classes.successIcon}`} />
                                            {success}
                                        </>
                                    )}
                                    {error && (
                                        <>
                                            <ErrorIcon className={`${classes.dropIcon} ${classes.errorIcon}`} />
                                            <Box>{error}</Box>
                                        </>
                                    )}
                                    <Box>{fileDetails}</Box>
                                </div>
                            </Paper>
                        </div>
                    </section>
                )}
            </Dropzone>
        </Box>
    );
};

export default ImageGalleryUpload;

const bytes = (size: number) => {
    return size;
};

const useStyles = makeStyles((theme) => ({
    root: {},
    dropzone: {
        position: 'relative',
        backgroundColor: theme.palette.grey[200],
        border: `2px dashed ${theme.palette.grey[400]}`,
        color: theme.palette.grey[500],
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        width: '100%',
    },
    dropzoneInner: {
        padding: theme.spacing(4, 2),
        textAlign: 'center',
        height: '100%',
        width: '100%',
    },
    dropIcon: {
        display: 'block',
        margin: '0 auto',
        fontSize: 40,
        marginBottom: theme.spacing(1),
    },
    successIcon: {
        color: green[500],
    },
    errorIcon: {
        color: red[500],
    },
}));

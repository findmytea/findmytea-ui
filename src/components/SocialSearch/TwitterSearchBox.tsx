import React from 'react';
import SocialSearchBox from './SocialSearchBox';
import { SocialItem } from '../../models/SocialItem';
import { searchTwitter } from '../../services/api/Twitter';

interface TwitterSearchBoxProps {
    initialValue: string;
    onSelect(item: SocialItem): void;
    disabled: boolean;
}

const TwitterSearchBox = (props: TwitterSearchBoxProps) => {
    const onSearch = (searchTerm: string, success: Function, error: Function) => {
        searchTwitter(searchTerm)
            .then((results) => {
                let items = Array<SocialItem>();
                for (let user of results.users) {
                    let item = new SocialItem();
                    item.id = String(user.id);
                    item.displayName = user.name;
                    item.name = user.screenName;
                    item.imageUrl = user.smallImageUrl;
                    item.description = user.description;
                    item.website = user.website;
                    items.push(item);
                }
                success(items);
            })
            .catch((e) => {
                error(e);
            });
    };

    return (
        <>
            <SocialSearchBox label="Twitter Handle" initialValue={props.initialValue} onSelect={props.onSelect} onSearch={onSearch} disabled={props.disabled}></SocialSearchBox>
        </>
    );
};

export default TwitterSearchBox;

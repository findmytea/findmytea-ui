import React, { useState } from 'react';
import { SocialItem } from '../../models/SocialItem';
import { TextField, CircularProgress, makeStyles, Box, Dialog, DialogTitle, Button, ListItem, ListItemAvatar, Avatar, ListItemText } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import logger from '../../lib/logger';
import Messages from '../Messages';

interface SocialSearchBoxProps {
    label: string;
    initialValue: string;
    onSelect(item: SocialItem): void;
    onSearch(searchTerm: string, success: Function, error: Function): void;
    disabled: boolean;
}

const SocialSearchBox = (props: SocialSearchBoxProps) => {
    const classes = smStyles();

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');
    const [info] = useState('');
    const [success] = useState('');

    const [open, setOpen] = useState(false);
    const [searchTerm, setSearchTerm] = useState('');
    const [options, setOptions] = useState(Array<SocialItem>());
    const [emptyResults, setEmptyResults] = useState(false);

    const onSearch = () => {
        logger.debug('onSearch(' + searchTerm + ')');
        setLoading(true);

        props.onSearch(
            searchTerm,
            (results: Array<SocialItem>) => {
                setOptions(results);
                setEmptyResults(results.length === 0);
                setLoading(false);
            },
            (e: any) => {
                setOptions(Array<SocialItem>());
                setError(e.message);
                setLoading(false);
            }
        );
    };

    const onOpen = (value: string) => {
        setOptions(Array<SocialItem>());
        setSearchTerm(value);
        setOpen(true);
    };

    const onSelect = (item: SocialItem) => {
        props.onSelect(item);
        onClose();
    };

    const onClose = () => {
        setOpen(false);
    };

    return (
        <>
            <TextField
                className={classes.smTextField}
                disabled={props.disabled}
                value={props.initialValue}
                label={props.label}
                variant="outlined"
                onChange={(evt) => onOpen(evt?.target?.value)}
                InputProps={{
                    endAdornment: (
                        <React.Fragment>
                            <button type="button" onClick={() => onOpen(props.initialValue)} disabled={props.disabled}>
                                <SearchIcon />
                            </button>
                        </React.Fragment>
                    ),
                }}
            />

            <Dialog aria-labelledby="simple-dialog-title" open={open} fullWidth>
                <Messages error={error} info={info} success={success}></Messages>
                <DialogTitle id="simple-dialog-title">
                    {props.label}
                    <Button onClick={onClose}>Close</Button>
                </DialogTitle>

                <TextField
                    autoFocus
                    className={classes.searchTextField}
                    disabled={loading || props.disabled}
                    value={searchTerm}
                    onChange={(evt: any) => setSearchTerm(evt?.target?.value)}
                    label={props.label}
                    variant="outlined"
                    InputProps={{
                        endAdornment: (
                            <React.Fragment>
                                {loading ? (
                                    <CircularProgress color="inherit" size={20} />
                                ) : (
                                    <button type="button" onClick={onSearch} disabled={props.disabled || !searchTerm || searchTerm.length === 0}>
                                        <SearchIcon />
                                    </button>
                                )}
                            </React.Fragment>
                        ),
                    }}
                />
                {emptyResults ? (
                    <Box>
                        <span>No results returned</span>
                    </Box>
                ) : (
                    ''
                )}

                {options.map((option: SocialItem, index: number) => {
                    var title = option.displayName;
                    if (option.displayName !== option.name) title += ' (' + option.name + ')';

                    return (
                        <ListItem button onClick={() => onSelect(option)} key={index}>
                            <ListItemAvatar>
                                <Avatar>
                                    <img id={String(option.id)} src={option.imageUrl} alt={option.displayName} hidden={!option.imageUrl} />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={title} secondary={option.description} />
                        </ListItem>
                    );
                })}
            </Dialog>
        </>
    );
};

const smStyles = makeStyles(() => {
    return {
        smTextField: {
            width: '100%',
            marginTop: '5px',
            marginBottom: '5px',
        },
        searchTextField: {
            width: '100%',
            paddingTop: '5px',
            paddingBottom: '5px',
            paddingLeft: '5px',
            paddingRight: '5px',
        },
    };
});

export default SocialSearchBox;

import React from 'react';
import SocialSearchBox from './SocialSearchBox';
import { SocialItem } from '../../models/SocialItem';
import { searchFacebook } from '../../services/api/FacebookApi';
import { Link } from '@material-ui/core';

interface FacebookSearchBoxProps {
    initialValue: string;
    link: string;
    onSelect(item: SocialItem): void;
    disabled: boolean;
}

const FacebookSearchBox = (props: FacebookSearchBoxProps) => {
    const onSearch = (searchTerm: string, success: Function, error: Function) => {
        searchFacebook(searchTerm)
            .then((results) => {
                let items = Array<SocialItem>();
                for (let page of results.pages) {
                    let item = new SocialItem();
                    item.id = String(page.id);
                    item.displayName = page.name;
                    item.name = page.name;
                    item.website = page.link;
                    item.description = page.about;
                    item.imageUrl = page.image;
                    items.push(item);
                }
                success(items);
            })
            .catch((e) => {
                error(e);
            });
    };

    return (
        <>
            <SocialSearchBox label="Facebook Page" initialValue={props.initialValue} onSelect={props.onSelect} onSearch={onSearch} disabled={props.disabled} />

            <Link className={props.link} href={props.link} target="_blank">
                {props.link}
            </Link>
        </>
    );
};

export default FacebookSearchBox;

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, Button, Typography, IconButton, Box } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import findMyIdentity from './../../lib/findMyIdentity';
import Session, { hasRole } from './../../models/Session';
import { ROUTES } from '../../config';
import { configureLoggerUser } from '../../lib/logger';
import { clearSession } from '../../actions/session';
import { mapStateToProps } from '../../reducers/session';

function mapDispatchToProps(dispatch: any) {
    return {
        clearSession: () => dispatch(clearSession()),
    };
}

interface AppProps {
    children: any;
    session: Session;
    clearSession: Function;
}

const App = (props: AppProps) => {
    const classes = useStyles();

    const onLogout = async (e: any) => {
        e.preventDefault();
        configureLoggerUser(undefined);
        findMyIdentity.logout();
        props.clearSession();
    };

    return (
        <div className={classes.root}>
            <AppBar position="static" className={classes.appBar}>
                <Toolbar>
                    <IconButton edge="start" color="inherit" aria-label="menu" className={classes.iconButton}>
                        <Link to="/">
                            <img src="/img/findmytea.jpg" alt="Find My Team" className={classes.logo} />
                        </Link>
                    </IconButton>
                    <Typography variant="h6" className={classes.title}></Typography>

                    <div>
                        {props.session.isLoggedIn ? (
                            <>
                                {hasRole(props.session.user, 'admin') ? (
                                    <>
                                        <Link color="inherit" to={ROUTES.BRANDS}>
                                            <Button color="inherit">Brands</Button>
                                        </Link>
                                        <Link color="inherit" to={ROUTES.LOCATIONS}>
                                            <Button color="inherit">Location</Button>
                                        </Link>
                                        <Link color="inherit" to={ROUTES.METRICS}>
                                            <Button color="inherit">Metrics</Button>
                                        </Link>
                                    </>
                                ) : (
                                    ''
                                )}
                                Logged in as {props.session?.user?.userName} |
                                <Button color="inherit" onClick={onLogout}>
                                    Logout
                                </Button>
                            </>
                        ) : (
                            <Button color="inherit" href={findMyIdentity.getLoginUri()}>
                                Login
                            </Button>
                        )}
                    </div>
                </Toolbar>
            </AppBar>
            <Box>
                <main>{props.children}</main>
            </Box>
        </div>
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

const useStyles = makeStyles((theme) => {
    return {
        root: {
            flexGrow: 1,
        },
        appBar: {
            backgroundColor: '#473c90',
            color: '#fff',
            zIndex: theme.zIndex.drawer + 1,
        },
        iconButton: {
            marginRight: theme.spacing(2),
        },

        logo: {
            height: 48,
        },
        title: {
            flexGrow: 1,
        },
    };
});

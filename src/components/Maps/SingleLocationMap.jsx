import React from 'react';

export default class SingleLocationMap extends React.Component {
    googleMapRef = React.createRef();

    componentDidMount() {
        let me = this;
        const googleMapScript = document.createElement('script');
        googleMapScript.src = `https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_GOOGLE_API_KEY}&libraries=places`;
        window.document.body.appendChild(googleMapScript);

        googleMapScript.addEventListener('load', () => {
            me.googleMap = this.createGoogleMap();
            me.marker = this.createMarker();
        });
    }

    componentDidUpdate(prevProps) {
        let me = this;
        if (me.googleMap && me.marker) {
            if (this.props.lng !== prevProps.lng || this.props.lat !== prevProps.lat) {
                const myLatlng = new window.google.maps.LatLng(this.props.lat, this.props.lng);
                me.marker.setPosition(myLatlng);
                me.googleMap.setCenter(myLatlng);
            }

            if (this.props.name !== prevProps.name) {
                me.marker.setLabel(this.props.name);
            }
        }
    }

    createGoogleMap = () =>
        new window.google.maps.Map(this.googleMapRef.current, {
            zoom: 18,
            center: {
                lat: 0,
                lng: 0,
            },
            disableDefaultUI: true,
        });

    createMarker = () =>
        new window.google.maps.Marker({
            map: this.googleMap,
        });

    render() {
        return <div id="google-map" ref={this.googleMapRef} style={{ width: '100%', height: '400px' }} />;
    }
}

import React from 'react';

import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props: any) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

interface MessagesProps {
    error: string;
    success: string;
    info: string;
}

export default function Messages(props: MessagesProps) {
    const open = (message: string): boolean => {
        return message !== undefined && message.length > 0;
    };

    return (
        <>
            <Snackbar open={open(props.error)}>
                <Alert severity="error">{props.error}</Alert>
            </Snackbar>
            <Snackbar open={open(props.success)}>
                <Alert severity="success">{props.success}</Alert>
            </Snackbar>
            <Snackbar open={open(props.info)}>
                <Alert severity="info">{props.info}</Alert>
            </Snackbar>
        </>
    );
}

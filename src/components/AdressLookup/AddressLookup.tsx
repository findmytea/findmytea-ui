import React, { useEffect, useState } from 'react';
import { Button, Dialog, DialogTitle, Box, makeStyles, List, ListItem, ListItemText, CircularProgress } from '@material-ui/core';
import { lookupAddress } from '../../services/api/AddressLookupApi';
import Messages from '../Messages';
import { Address } from '../../models/Address';

interface AddressLookup {
    postcode: string;
    onSelect(addressLine1: string, addressLines2: string, town: string, county: string, postcode: string): void;
}

export default function AddressLookup(props: AddressLookup) {
    const classes = useStyles();
    const [open, setOpen] = useState(false);
    const [postcodeValid, setPostcodeValid] = useState(false);
    const [addresses, setAddresses] = useState(Array<Address>());
    const [loading, setLoading] = useState(false);

    const [error, setError] = useState('');
    const [info, setInfo] = useState('');
    const [success, setSuccess] = useState('');

    useEffect(() => {
        setPostcodeValid(!!props.postcode && props.postcode.match(/^[A-z]{1,2}[0-9][0-9A-z]?\s?[0-9][A-z]{2}$/) != null);
    }, [props.postcode]);

    const onLookup = async () => {
        setAddresses(Array<Address>());
        setLoading(true);
        setOpen(true);
        setInfo('Searching');
        setError('');
        setSuccess('');
        try {
            const response = await lookupAddress(props.postcode);
            setAddresses(response);
            setInfo('');
        } catch (e) {
            setError(e.message);
        } finally {
            setInfo('');
            setLoading(false);
        }
    };

    const onSelect = (address: Address) => {
        props.onSelect(address.addressLine1, address.addressLine2, address.townCity, address.county, address.postcode);
        setOpen(false);
    };

    const onClose = () => {
        setOpen(false);
    };

    return (
        <>
            <Button variant="contained" color="primary" disabled={!postcodeValid} onClick={onLookup}>
                Lookup Address
            </Button>

            <Dialog aria-labelledby="simple-dialog-title" open={open}>
                <Messages error={error} info={info} success={success}></Messages>
                <DialogTitle id="simple-dialog-title">
                    Postcode: {props.postcode} <Button onClick={onClose}>Close</Button>
                </DialogTitle>

                <Box className={classes.lookupBox}>
                    <Box>
                        {loading ? (
                            <CircularProgress color="inherit" size={20} />
                        ) : (
                            <List dense className={classes.addressList}>
                                {addresses.map((address, index) => {
                                    return (
                                        <ListItem button onClick={() => onSelect(address)} key={index}>
                                            <ListItemText primary={format(address)} />
                                        </ListItem>
                                    );
                                })}
                            </List>
                        )}
                    </Box>
                </Box>
            </Dialog>
        </>
    );
}

const format = (address: Address) => {
    return [address.organisation, address.addressLine1, address.addressLine2, address.townCity, address.county, address.postcode]
        .filter((x) => typeof x === 'string' && x.length > 0)
        .join(', ');
};

const useStyles = makeStyles(() => {
    return {
        lookupBox: {
            minWidth: '200px',
        },
        addressList: {},
    };
});

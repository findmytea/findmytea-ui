import React, { useState, useEffect } from 'react';
import LocationDetail from '../../models/LocationDetail';
import { useFormik } from 'formik';
import { Snackbar, Fab } from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';

import LocationDetails from './LocationDetails';
import LocationLocation from './LocationLocation';
import LocationSocial from './LocationSocial';
import LocationImages from './LocationImages';
import LocationBrands from './LocationBrands';
import { FieldGroup } from '../FieldGroup';
import { Prompt } from 'react-router';
import * as yup from 'yup';

interface LocationFormProps {
    location: LocationDetail;
    onSubmit(location: LocationDetail): any;
    disabled: boolean;
}

const LocationForm = (props: LocationFormProps) => {
    const [initialValues, setInitialValues] = useState<LocationDetail | null>();

    useEffect(() => {
        setInitialValues(props.location);
    }, [props]);

    const validationSchema = yup.object().shape({
        // eslint-disable-next-line
        name: yup.string().required('Name required.'),
        email: yup.string().nullable().email('Valid email required'),
        website: yup.string().nullable().url('Valid url required'),
    });

    const formik = useFormik({
        initialValues: Object.assign({}, initialValues),
        enableReinitialize: true,
        validationSchema: validationSchema,
        validateOnBlur: true,
        onSubmit: async (values) => {
            try {
                const response = await props.onSubmit(Object.assign(new LocationDetail(), values));
                setInitialValues(response);
            } catch (error) {}
        },
    });

    const submitDisabled = (formik: any) => {
        return props.disabled || !formik.dirty || !formik.isValid;
    };

    return (
        <>
            <Prompt when={formik.dirty} message="You have unsaved changes that will be lost if you navigate away from this form. Are you sure?" />
            <h1>{formik?.values?.name}</h1>
            <p>{formik.dirty ? 'You have unsaved changes.' : ''}</p>

            <form onSubmit={formik.handleSubmit}>
                <FieldGroup title="Details">
                    <LocationDetails formik={formik} disabled={props.disabled} />
                </FieldGroup>

                <FieldGroup title="Location">
                    <LocationLocation formik={formik} disabled={props.disabled} />
                </FieldGroup>

                <FieldGroup title="Social Media">
                    <LocationSocial formik={formik} disabled={props.disabled} />
                </FieldGroup>

                <FieldGroup title="Images">
                    <LocationImages formik={formik} disabled={props.disabled} />
                </FieldGroup>

                <FieldGroup title="Brands">
                    <LocationBrands formik={formik} disabled={props.disabled} />
                </FieldGroup>

                <Snackbar open={true} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                    <Fab color="primary" aria-label="Dave" type="submit" disabled={submitDisabled(formik)}>
                        <SaveIcon />
                    </Fab>
                </Snackbar>
            </form>
        </>
    );
};

export default LocationForm;

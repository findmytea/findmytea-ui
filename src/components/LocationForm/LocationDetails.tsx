import React from 'react';
import { Box, TextField, FormGroup, FormControlLabel, Checkbox } from '@material-ui/core';
import locationStyles from './locationStyles';
import { emptyIfNull, setFieldValue, falseIfNull, setFieldChecked } from './../../lib/formUtils';
import { FormProps } from './../../lib/formProps';
import { get } from '../../lib/keypather';

const LocationDetails = (props: FormProps) => {
    const classes = locationStyles();

    return (
        <>
            <Box className={classes.locationBox}>
                <TextField
                    className={classes.locationTextField}
                    id="location-name"
                    onChange={(evt) => setFieldValue(props, evt, 'name')}
                    value={emptyIfNull(props.formik.values.name)}
                    label="Name"
                    variant="outlined"
                    required={true}
                    disabled={props.disabled}
                    error={get(props.formik.errors, 'name') != null}
                    helperText={get(props.formik.errors, 'name')}
                    InputProps={{}}
                />
            </Box>

            <Box className={classes.locationBox}>
                <TextField
                    className={classes.locationTextField}
                    id="location-description"
                    onChange={(evt) => setFieldValue(props, evt, 'description')}
                    value={emptyIfNull(props.formik.values.description)}
                    label="Description"
                    variant="outlined"
                    multiline
                    rows={3}
                    disabled={props.disabled}
                    error={get(props.formik.errors, 'description') != null}
                    helperText={get(props.formik.errors, 'description')}
                    InputProps={{}}
                />
            </Box>

            <Box className={classes.locationBox}>
                <TextField
                    className={classes.locationTextField}
                    id="location-website"
                    onChange={(evt) => setFieldValue(props, evt, 'website')}
                    value={emptyIfNull(props.formik.values.website)}
                    label="Website"
                    variant="outlined"
                    disabled={props.disabled}
                    error={get(props.formik.errors, 'website') != null}
                    helperText={get(props.formik.errors, 'website')}
                    InputProps={{}}
                />
            </Box>

            <Box className={classes.locationBox}>
                <TextField
                    className={classes.locationTextField}
                    id="location-phone"
                    onChange={(evt) => setFieldValue(props, evt, 'phone')}
                    value={emptyIfNull(props.formik.values.phone)}
                    label="Phone"
                    variant="outlined"
                    disabled={props.disabled}
                    error={get(props.formik.errors, 'phone') != null}
                    helperText={get(props.formik.errors, 'phone')}
                    InputProps={{}}
                />
            </Box>

            <Box className={classes.locationBox}>
                <TextField
                    className={classes.locationTextField}
                    id="location-email"
                    onChange={(evt) => setFieldValue(props, evt, 'email')}
                    value={emptyIfNull(props.formik.values.email)}
                    label="Email"
                    variant="outlined"
                    disabled={props.disabled}
                    error={get(props.formik.errors, 'email') != null}
                    helperText={get(props.formik.errors, 'email')}
                    InputProps={{}}
                />
            </Box>

            <Box className={classes.locationBox}>
                <FormGroup>
                    <FormControlLabel
                        control={
                            <Checkbox
                                disabled={props.disabled}
                                checked={falseIfNull(props.formik.values.cafe)}
                                onChange={(evt) => setFieldChecked(props, evt, 'cafe')}
                                id="location-cafe"
                            />
                        }
                        label="Cafe"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                disabled={props.disabled}
                                checked={falseIfNull(props.formik.values.retailer)}
                                onChange={(evt) => setFieldChecked(props, evt, 'retailer')}
                                id="location-retailer"
                            />
                        }
                        label="Retailer"
                    />
                </FormGroup>
            </Box>
        </>
    );
};

export default LocationDetails;

import { makeStyles } from '@material-ui/core';

const locationStyles = makeStyles(() => {
    return {
        locationBox: {
            padding: '10px',
        },
        locationTextField: {
            width: '100%',
            marginTop: '5px',
            marginBottom: '5px',
        },
        image: {
            height: 200,
        },
        brandList: {},
    };
});

export default locationStyles;

import React from 'react';
import { Box, TextField } from '@material-ui/core';
import { FormProps } from './../../lib/formProps';
import { SingleLocationMap } from '../Maps';
import locationStyles from './locationStyles';
import { emptyIfNull, setFieldValue, setFieldNumber, zeroIfNull } from './../../lib/formUtils';
import AddressLookup from '../AdressLookup/AddressLookup';
import logger from '../../lib/logger';
import { geoCode } from '../../services/api/GeoCodeApi';
import { get } from '../../lib/keypather';

const LocationLocation = (props: FormProps) => {
    const classes = locationStyles();

    const onSelectAddress = async (addressLine1: string, addressLine2: string, town: string, county: string, postcode: string) => {
        props.formik.setFieldValue('addressLine1', addressLine1);
        props.formik.setFieldValue('addressLines2', addressLine2);
        props.formik.setFieldValue('town', town);
        props.formik.setFieldValue('county', county);
        props.formik.setFieldValue('postcode', postcode);

        try {
            const address = [addressLine1, addressLine2, town, county, postcode].filter((x) => typeof x === 'string' && x.length > 0).join(', ');

            const response = await geoCode(address);
            props.formik.setFieldValue('geoX', response.geoX);
            props.formik.setFieldValue('geoY', response.geoY);
        } catch (e) {
            logger.error(e.message);
        }
    };

    return (
        <>
            <Box className={classes.locationBox}>
                <TextField
                    className={classes.locationTextField}
                    id="location-addressLine1"
                    onChange={(evt) => setFieldValue(props, evt, 'addressLine1')}
                    value={emptyIfNull(props.formik.values.addressLine1)}
                    label="Address Line 1"
                    variant="outlined"
                    disabled={props.disabled}
                    error={get(props.formik.errors, 'addressLine1') != null}
                    helperText={get(props.formik.errors, 'addressLine2')}
                    InputProps={{}}
                />
            </Box>

            <Box className={classes.locationBox}>
                <TextField
                    className={classes.locationTextField}
                    id="location-addressLine2"
                    onChange={(evt) => setFieldValue(props, evt, 'addressLine2')}
                    value={emptyIfNull(props.formik.values.addressLine2)}
                    label="Address Line 2"
                    variant="outlined"
                    disabled={props.disabled}
                    error={get(props.formik.errors, 'addressLine2') != null}
                    helperText={get(props.formik.errors, 'addressLine2')}
                    InputProps={{}}
                />
            </Box>

            <Box className={classes.locationBox}>
                <TextField
                    className={classes.locationTextField}
                    id="location-town"
                    onChange={(evt) => setFieldValue(props, evt, 'town')}
                    value={emptyIfNull(props.formik.values.town)}
                    label="Town"
                    variant="outlined"
                    disabled={props.disabled}
                    error={get(props.formik.errors, 'town') != null}
                    helperText={get(props.formik.errors, 'town')}
                    InputProps={{}}
                />
            </Box>

            <Box className={classes.locationBox}>
                <TextField
                    className={classes.locationTextField}
                    id="location-county"
                    onChange={(evt) => setFieldValue(props, evt, 'county')}
                    value={emptyIfNull(props.formik.values.county)}
                    label="County"
                    variant="outlined"
                    disabled={props.disabled}
                    error={get(props.formik.errors, 'county') != null}
                    helperText={get(props.formik.errors, 'county')}
                    InputProps={{}}
                />
            </Box>

            <Box className={classes.locationBox}>
                <TextField
                    className={classes.locationTextField}
                    id="location-postcode"
                    onChange={(evt) => setFieldValue(props, evt, 'postcode')}
                    value={emptyIfNull(props.formik.values.postcode)}
                    label="Postcode"
                    variant="outlined"
                    disabled={props.disabled}
                    error={get(props.formik.errors, 'postcode') != null}
                    helperText={get(props.formik.errors, 'postcode')}
                    InputProps={{}}
                />
                <AddressLookup onSelect={onSelectAddress} postcode={props.formik.values.postcode} />
            </Box>

            <Box className={classes.locationBox}>
                <TextField
                    className={classes.locationTextField}
                    id="location-x"
                    type="number"
                    onChange={(evt) => setFieldNumber(props, evt, 'geoX')}
                    value={zeroIfNull(props.formik.values.geoX)}
                    label="Lat"
                    variant="outlined"
                    disabled={props.disabled}
                    error={get(props.formik.errors, 'geoX') != null}
                    helperText={get(props.formik.errors, 'geoX')}
                    InputProps={{ readOnly: true }}
                />
            </Box>

            <Box className={classes.locationBox}>
                <TextField
                    className={classes.locationTextField}
                    id="location-twitterId"
                    type="number"
                    onChange={(evt) => setFieldNumber(props, evt, 'geoY')}
                    value={zeroIfNull(props.formik.values.geoY)}
                    label="Lng"
                    variant="outlined"
                    error={get(props.formik.errors, 'geoY') != null}
                    helperText={get(props.formik.errors, 'geoY')}
                    InputProps={{ readOnly: true }}
                />
            </Box>

            <Box className={classes.locationBox}>
                <SingleLocationMap lat={props.formik.values.geoX} lng={props.formik.values.geoY} name={props.formik.values.name} />
            </Box>
        </>
    );
};

export default LocationLocation;

import React, { useState, useEffect } from 'react';
import { Box, List, ListItem, ListItemIcon, Checkbox, ListItemAvatar, Avatar, ListItemText } from '@material-ui/core';
import { FormProps } from './../../lib/formProps';
import locationStyles from './locationStyles';
import Brand from '../../models/Brand';
import { getBrands } from '../../services/api/BrandApi';

const LocationBrands = (props: FormProps) => {
    const classes = locationStyles();
    const [brands, setBrands] = useState(Array<Brand>());

    useEffect(() => {
        getBrands()
            .then((response) => {
                setBrands(response.brands);
            })
            .catch((error: any) => {
                console.log(error.message);
            });
    }, []);

    const findBrand = (brand: Brand): number => {
        for (let index in props.formik.values.brands) if (props.formik.values.brands[index].id === brand.id) return +index;

        return -1;
    };

    const onClickBrand = (brand: Brand) => {
        const brands = Object.assign(new Array<Brand>(), props.formik.values.brands);
        const index = findBrand(brand);

        if (index !== -1) brands.splice(index, 1);
        else brands.push(brand);

        props.formik.setFieldValue('brands', brands);
    };

    return (
        <>
            <Box>
                <List className={classes.brandList}>
                    {brands.map((brand) => {
                        const labelId = `checkbox-list-secondary-label-${brand.id}`;
                        return (
                            <ListItem key={brand.id}>
                                <ListItemIcon>
                                    <Checkbox
                                        edge="end"
                                        onChange={(evt) => onClickBrand(brand)}
                                        checked={findBrand(brand) !== -1}
                                        disableRipple
                                        inputProps={{
                                            'aria-labelledby': labelId,
                                        }}
                                    />
                                </ListItemIcon>
                                <a href={brand.website} target="_blank" rel="noopener noreferrer">
                                    <ListItemAvatar>
                                        <Avatar alt={brand.name} src={brand?.image?.uri} />
                                    </ListItemAvatar>
                                </a>
                                <a href={brand.website} target="_blank" rel="noopener noreferrer">
                                    <ListItemText id={labelId} primary={brand.name} />
                                </a>
                            </ListItem>
                        );
                    })}
                </List>
            </Box>
        </>
    );
};

export default LocationBrands;

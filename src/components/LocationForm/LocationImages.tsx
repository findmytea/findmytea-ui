import React from 'react';
import { Box } from '@material-ui/core';
import { FormProps } from './../../lib/formProps';
import locationStyles from './locationStyles';
import { ImageGallery } from '../ImageGallery';
import Image from '../../models/Image';
import Img from '../Image';

const LocationImages = (props: FormProps) => {
    const classes = locationStyles();

    const onSelectImage = (image: Image) => {
        props.formik.setFieldValue('image', image);
    };

    const onImageUploaded = (image: Image) => {
        const images = props.formik.values.images;
        images.push(image);
        props.formik.setFieldValue('images', images);
    };

    return (
        <>
            <Box className={classes.locationBox}>
                <Img src={props?.formik?.values?.image?.uri} alt={props.formik.values.name} className={classes.image} />
                <Box>
                    <ImageGallery images={props.formik.values.images} onSelect={onSelectImage} onUploaded={onImageUploaded} disabled={props.disabled} />
                </Box>
            </Box>
        </>
    );
};

export default LocationImages;

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Brand from '../../models/Brand';
import { Grid } from '@material-ui/core';
import Image from '../Image';

interface BrandGallerProp {
    brands: Array<Brand>;
    imageHeight: number;
}

export default function BrandGallery(props: BrandGallerProp) {
    const classes = useStyles();

    return (
        <Grid container spacing={1}>
            {props.brands.map((brand: Brand, index: number) => {
                return (
                    <Grid item xs={3} sm={2} xl={1} key={index}>
                        <a href={brand.website} target="_blank" rel="noopener noreferrer">
                            <Image className={classes.brandImages} src={brand.image?.uri} alt={brand.name} title={brand.name} height={props.imageHeight} />
                        </a>
                    </Grid>
                );
            })}
        </Grid>
    );
}

const useStyles = makeStyles({
    brandImages: {
        marginLeft: 2,
        marginRight: 2,
        marginTop: 2,
        marginBottom: 2,
    },
});

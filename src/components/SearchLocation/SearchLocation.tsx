import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, InputAdornment } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';

interface AppSearchLocationProps {
    onSearch(searchTerm: string): void;
    searching: boolean;
}

export default function SearchLocation(props: AppSearchLocationProps) {
    const classes = useStyles();
    const [value, setValue] = useState('');

    const onChangeValue = (e: any) => {
        setValue(e.target.value);
    };

    const submit = (values: any, e: any) => {
        e && e.preventDefault();
        props.onSearch(value);
    };

    const searchDisabled = (): boolean => {
        return !value || value.length <= 1 || props.searching;
    };

    return (
        <>
            <form
                noValidate
                autoComplete="off"
                onSubmit={(e) => submit(value, e)}
            >
                <TextField
                    name="s_search"
                    label="Where are you?"
                    variant="outlined"
                    size="small"
                    margin="normal"
                    fullWidth
                    value={value}
                    onChange={onChangeValue}
                    disabled={props.searching}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <button
                                    type="submit"
                                    className={classes.button}
                                    disabled={searchDisabled()}
                                >
                                    <SearchIcon />
                                </button>
                            </InputAdornment>
                        ),
                    }}
                />
            </form>
        </>
    );
}

/*

 value={values.s_text}
    onChange={onChangeValues}
    onBlur={onBlur}
    onFocus={onFocus}
    disabled={disabled}

    */

const useStyles = makeStyles((theme) => ({
    select: {
        minWidth: 120,
    },
    button: {
        background: 'transparent',
        border: 'none',
        padding: 0,
    },
}));

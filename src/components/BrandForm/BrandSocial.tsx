import React from 'react';
import { Box } from '@material-ui/core';
import { FormProps } from './../../lib/formProps';
import brandStyles from './brandStyles';
import { emptyIfNull } from './../../lib/formUtils';
import { TwitterSearchBox, FacebookSearchBox } from '../SocialSearch';
import { SocialItem } from '../../models/SocialItem';

const brandSocial = (props: FormProps) => {
    const classes = brandStyles();

    const onTwitterSelect = (item: SocialItem) => {
        props.formik.setFieldValue('twitterId', +item.id);
        props.formik.setFieldValue('twitterHandle', item.name);
    };

    const onFacebookSelect = (item: SocialItem) => {
        props.formik.setFieldValue('facebookId', +item.id);
        props.formik.setFieldValue('facebookName', item.name);
        props.formik.setFieldValue('facebookLink', item.website);
    };

    return (
        <>
            <Box className={classes.brandBox}>
                <TwitterSearchBox initialValue={emptyIfNull(props.formik.values.twitterHandle)} onSelect={onTwitterSelect} disabled={props.disabled}></TwitterSearchBox>
            </Box>

            <Box className={classes.brandBox}>
                <FacebookSearchBox
                    link={emptyIfNull(props.formik.values.facebookLink)}
                    initialValue={emptyIfNull(props.formik.values.facebookName)}
                    onSelect={onFacebookSelect}
                    disabled={props.disabled}
                ></FacebookSearchBox>
            </Box>
        </>
    );
};

export default brandSocial;

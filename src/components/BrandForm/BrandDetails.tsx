import React from 'react';
import { FormProps } from './../../lib/formProps';
import brandStyles from './brandStyles';
import { Box, TextField } from '@material-ui/core';
import { setFieldValue, emptyIfNull } from '../../lib/formUtils';

import { get } from './../../lib/keypather';

const BrandDetails = (props: FormProps) => {
    const classes = brandStyles();

    return (
        <>
            <Box className={classes.brandBox}>
                <TextField
                    className={classes.brandTextField}
                    id="brand-name"
                    onChange={(evt) => setFieldValue(props, evt, 'name')}
                    value={emptyIfNull(get(props.formik.values, 'name'))}
                    label="Name"
                    variant="outlined"
                    required={true}
                    disabled={props.disabled}
                    error={get(props.formik.errors, 'name') != null}
                    helperText={get(props.formik.errors, 'name')}
                    InputProps={{}}
                />
            </Box>

            <Box className={classes.brandBox}>
                <TextField
                    className={classes.brandTextField}
                    id="brand-description"
                    onChange={(evt) => setFieldValue(props, evt, 'description')}
                    value={emptyIfNull(props.formik.values.description)}
                    label="Description"
                    variant="outlined"
                    multiline
                    rows={3}
                    disabled={props.disabled}
                    error={get(props.formik.errors, 'description') != null}
                    helperText={get(props.formik.errors, 'description')}
                    InputProps={{}}
                />
            </Box>

            <Box className={classes.brandBox}>
                <TextField
                    className={classes.brandTextField}
                    id="brand-website"
                    onChange={(evt) => setFieldValue(props, evt, 'website')}
                    value={emptyIfNull(props.formik.values.website)}
                    label="Website"
                    variant="outlined"
                    disabled={props.disabled}
                    error={get(props.formik.errors, 'website') != null}
                    helperText={get(props.formik.errors, 'website')}
                    InputProps={{}}
                />
            </Box>

            <Box className={classes.brandBox}>
                <TextField
                    className={classes.brandTextField}
                    id="brand-phone"
                    onChange={(evt) => setFieldValue(props, evt, 'phone')}
                    value={emptyIfNull(props.formik.values.phone)}
                    label="Phone"
                    variant="outlined"
                    disabled={props.disabled}
                    error={get(props.formik.errors, 'phone') != null}
                    helperText={get(props.formik.errors, 'phone')}
                    InputProps={{}}
                />
            </Box>

            <Box className={classes.brandBox}>
                <TextField
                    className={classes.brandTextField}
                    id="brand-email"
                    onChange={(evt) => setFieldValue(props, evt, 'email')}
                    value={emptyIfNull(props.formik.values.email)}
                    label="Email"
                    variant="outlined"
                    disabled={props.disabled}
                    error={get(props.formik.errors, 'email') != null}
                    helperText={get(props.formik.errors, 'email')}
                    InputProps={{}}
                />
            </Box>
        </>
    );
};

export default BrandDetails;

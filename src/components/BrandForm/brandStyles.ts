import { makeStyles } from '@material-ui/core';

const brandStyles = makeStyles(() => {
    return {
        brandBox: {
            padding: '10px',
        },
        brandTextField: {
            width: '100%',
            marginTop: '5px',
            marginBottom: '5px',
        },
        image: {
            height: 200,
        },
    };
});

export default brandStyles;

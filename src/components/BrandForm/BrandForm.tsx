import React, { useState, useEffect } from 'react';
import BrandDetail from '../../models/Brand';
import { useFormik } from 'formik';
import { Snackbar, Fab } from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import BrandDetails from './BrandDetails';
import BrandSocial from './BrandSocial';
import BrandImages from './BrandImages';
import * as yup from 'yup';
import { checkName } from '../../services/api/BrandApi';
import { Prompt } from 'react-router';
import { FieldGroup } from '../FieldGroup';

interface BrandFormProps {
    brand: BrandDetail;
    onSubmit(location: BrandDetail): any;
    disabled: boolean;
}

const BrandForm = (props: BrandFormProps) => {
    const [initialValues, setInitialValues] = useState<BrandDetail | null>();
    const [initialName, setInitialName] = useState<string | null>(null);

    useEffect(() => {
        setInitialName(props?.brand?.name);
        setInitialValues(props.brand);
    }, [props]);

    const isNameUnique = async (value: string) => {
        if (initialName === null) return true;

        if (initialName === value) return true;

        const result = await checkName(value);
        return !result.value;
    };

    const validationSchema = yup.object().shape({
        // eslint-disable-next-line
        name: yup.string().required('Name required.').test('uniqueName', "The name '${value}' already being used.", isNameUnique),
        email: yup.string().nullable().email('Valid email required'),
        website: yup.string().nullable().url('Valid url required'),
    });

    const formik = useFormik({
        initialValues: Object.assign({}, initialValues),
        enableReinitialize: true,
        validationSchema: validationSchema,
        validateOnBlur: true,
        onSubmit: async (values) => {
            try {
                const response = await props.onSubmit(Object.assign(new BrandDetail(), values));
                setInitialValues(response);
            } catch (error) {}
        },
    });

    const submitDisabled = (formik: any) => {
        return props.disabled || !formik.dirty || !formik.isValid;
    };

    return (
        <>
            <Prompt when={formik.dirty} message="You have unsaved changes that will be lost if you navigate away from this form. Are you sure?" />
            <h1>{formik?.values?.name}</h1>
            <p>{formik.dirty ? 'You have unsaved changes.' : ''}</p>

            <form onSubmit={formik.handleSubmit}>
                <FieldGroup title="Details">
                    <BrandDetails formik={formik} disabled={props.disabled} />
                </FieldGroup>
                <FieldGroup title="Social">
                    <BrandSocial formik={formik} disabled={props.disabled} />
                </FieldGroup>
                <FieldGroup title="Images">
                    <BrandImages formik={formik} disabled={props.disabled} />
                </FieldGroup>

                <Snackbar open={true} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
                    <Fab color="primary" aria-label="Dave" type="submit" disabled={submitDisabled(formik)}>
                        <SaveIcon />
                    </Fab>
                </Snackbar>
            </form>
        </>
    );
};

export default BrandForm;

import React from 'react';
import { Paper, makeStyles, Typography } from '@material-ui/core';

interface SubTitleProps {
    children: any;
}

function SubTitle(props: SubTitleProps) {
    const classes = useStyles(props);
    return (
        <Typography variant="subtitle1" component={'h1'} className={classes.title}>
            {props.children}
        </Typography>
    );
}

interface FieldGroupProps {
    children: any;
    title: string;
}

export function FieldGroup(props: FieldGroupProps) {
    const classes = useStyles(props);
    return (
        <Paper className={classes.paper}>
            <SubTitle>{props.title}</SubTitle>
            {props.children}
        </Paper>
    );
}

const useStyles = makeStyles((theme) => ({
    title: {
        textTransform: 'uppercase',
        fontWeight: 600,
        marginBottom: theme.spacing(2),
        marginTop: theme.spacing(2),
        lineHeight: 1.3,
    },
    paper: {
        marginTop: 0,
        marginBottom: theme.spacing(2),
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
    },
}));

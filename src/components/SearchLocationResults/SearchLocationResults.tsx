import React from 'react';
import Location from '../../models/Location';
import LocationResponse from '../../models/LocationResponse';
import SearchLocationResultItem from '../SearchLocationResultItem';

interface SearchLocationResultsProps {
    results: LocationResponse;
}

export default function SearchLocationResults(
    props: SearchLocationResultsProps
) {
    return (
        <>
            {props.results.locations.map((value: Location, index: number) => {
                return (
                    <SearchLocationResultItem
                        location={value}
                        key={index}
                    ></SearchLocationResultItem>
                );
            })}
        </>
    );
}

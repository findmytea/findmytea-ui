import React from 'react';
import Location from './../../models/Location';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/ButtonBase';
import Fab from '@material-ui/core/Fab';
import Storefront from '@material-ui/icons/Storefront';
import LocalCafe from '@material-ui/icons/LocalCafe';
import BrandGallery from '../BrandGallery/BrandGallery';
import Image from '../Image';
import { address } from '../../lib/addressUtils';

interface SearchLocationResultItemProp {
    location: Location;
}

export default function SearchLocationResultItem(props: SearchLocationResultItemProp) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <Grid container spacing={2}>
                    <Grid item>
                        <Button className={classes.image} href={props.location.website} target="_blank" rel="noopener noreferrer">
                            <Image className={classes.img} alt={props?.location?.name} src={props?.location?.image?.uri} />
                        </Button>
                    </Grid>
                    <Grid item xs={12} sm container>
                        <Grid item xs container direction="column" spacing={2}>
                            <Grid item xs>
                                <Typography gutterBottom variant="subtitle1">
                                    {props.location.name}
                                </Typography>
                                <Typography variant="body2" color="textSecondary">
                                    {address(props.location)}
                                </Typography>
                                <Typography variant="body2" gutterBottom>
                                    <a href={props.location.website} target="_blank" rel="noopener noreferrer">
                                        website
                                    </a>
                                </Typography>
                                <BrandGallery brands={props.location?.brands} imageHeight={25} />
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Typography variant="subtitle1">{format(props.location.distance)}</Typography>

                            <ul className={classes.locationTypeGroup}>
                                {props.location.cafe ? (
                                    <li className={classes.locationTypeIcon}>
                                        <Fab color="primary" size="small" aria-label="Cafe">
                                            <LocalCafe />
                                        </Fab>
                                    </li>
                                ) : (
                                    ''
                                )}
                                {props.location.retailer ? (
                                    <li className={classes.locationTypeIcon}>
                                        <Fab color="primary" size="small" aria-label="Retailer">
                                            <Storefront />
                                        </Fab>
                                    </li>
                                ) : (
                                    ''
                                )}
                            </ul>
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>
        </div>
    );
}

const format = (distance: number): string => {
    if (distance > 999) return `${(distance / 1000).toFixed(2)} Km`;
    else return `${distance} m`;
};

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
    },
    image: {
        width: 100,
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },
    locationTypeGroup: {
        paddingLeft: 0,
        listStyle: 'none',
    },
    locationTypeIcon: {
        display: 'inline-block',
        paddingLeft: 5,
        paddingRight: 5,
    },
    brandImages: {
        marginLeft: 2,
        marginRight: 2,
        marginTop: 2,
        marginBottom: 2,
        height: 25,
    },
}));

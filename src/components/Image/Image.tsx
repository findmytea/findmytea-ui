import React from 'react';

interface ImageProps {
    className?: string;
    src?: string;
    alt?: string;
    title?: string;
    height?: number;
}

const Img = (props: ImageProps) => {
    return <img className={props.className} alt={props.alt} src={props.src ? props.src : '/img/findmytea.png'} title={props.title} height={props.height} />;
};

export default Img;

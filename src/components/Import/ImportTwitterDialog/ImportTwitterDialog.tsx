import React, { useState, useEffect } from 'react';
import { Button, Dialog, DialogTitle, Box, makeStyles, TextField, Link } from '@material-ui/core';
import { TwitterSearchBox } from '../../SocialSearch';
import { SocialItem } from '../../../models/SocialItem';
import Img from '../../Image/Image';

interface ImportTwitterDialog {
    open: boolean;
    onImport(user: SocialItem): void;
    onClose(): void;
}

const ImportTwitterDialog = (props: ImportTwitterDialog) => {
    const classes = useStyles();

    const [twitterUser, setTwitterUser] = useState<SocialItem>(new SocialItem());

    useEffect(() => {
        if (props.open) setTwitterUser(new SocialItem());
    }, [props.open]);

    const onSelect = (item: SocialItem) => {
        setTwitterUser(item);
    };

    return (
        <>
            <Dialog aria-labelledby="import-twitter-dialog-title" open={props.open}>
                <DialogTitle id="import-twitter-dialog-title">
                    <span className={classes.dialogTitle}>Import from Twitter...</span>
                    <Button disabled={!twitterUser.id} onClick={() => props.onImport(twitterUser)}>
                        Import
                    </Button>
                    <Button onClick={props.onClose}>Close</Button>
                </DialogTitle>
                <Box className={classes.dialogBody}>
                    <Box className={classes.box}>
                        <TwitterSearchBox disabled={false} initialValue={twitterUser.name} onSelect={onSelect} />
                    </Box>

                    <Box className={classes.box}>
                        <TextField className={classes.textField} value={twitterUser.displayName} label="Name" variant="outlined" InputProps={{ readOnly: true }} />
                    </Box>
                    <Box className={classes.box}>
                        <Link className={classes.link} href={twitterUser.website} target="_blank">
                            {twitterUser?.website ?? ''}
                        </Link>
                    </Box>
                    <Box className={classes.box}>
                        <TextField
                            className={classes.textField}
                            value={twitterUser?.description}
                            label="Description"
                            variant="outlined"
                            multiline={true}
                            rows={3}
                            InputProps={{ readOnly: true }}
                        />
                    </Box>
                    <Box className={classes.box} onClick={() => props.onImport(twitterUser)}>
                        <Img src={twitterUser.imageUrl} className={classes.image} />
                    </Box>
                </Box>
            </Dialog>
        </>
    );
};

export default ImportTwitterDialog;

const useStyles = makeStyles((theme) => ({
    dialogBody: {
        width: 450,
    },
    dialogTitle: {
        marginRight: theme.spacing(2),
    },
    box: {
        padding: '10px',
    },
    textField: { width: '100%' },
    image: {
        width: 65,
        height: 65,
    },
    link: { marginLeft: 5 },
}));

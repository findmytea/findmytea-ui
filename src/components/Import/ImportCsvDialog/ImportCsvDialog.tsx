import React, { useState, useEffect } from 'react';
import { Dialog, DialogTitle, Button, Box, makeStyles, Paper, CircularProgress } from '@material-ui/core';
import FileIcon from '@material-ui/icons/InsertDriveFile';
import CheckIcon from '@material-ui/icons/Check';
import ErrorIcon from '@material-ui/icons/Error';
import { green, red } from '@material-ui/core/colors';
import Dropzone from 'react-dropzone';

interface ImportCsvDialogProps {
    open: boolean;
    onImport(file: any): void;
    onClose(): void;
}

const ImportCsvDialog = (props: ImportCsvDialogProps) => {
    const classes = useStyles();

    const [file, setFile] = useState();
    const [pending, setPending] = useState(false);
    const [fileDetails, setFileDetails] = useState('');
    const [error, setError] = useState(false);
    const [success, setSuccess] = useState(false);

    useEffect(() => {
        if (props.open) {
            setFile(undefined);
            setSuccess(true);
        }
    }, [props.open]);

    const onDrop = async (acceptedFiles: any) => {
        setFile(acceptedFiles[0]);

        setSuccess(false);
        setError(false);
        setFileDetails(`Uploading ${acceptedFiles[0].name} (${bytes(acceptedFiles[0].size)})`);
        setPending(true);

        try {
            await props.onImport(acceptedFiles[0]);
            setSuccess(true);
            setFile(undefined);
            props.onClose();
        } catch (e) {
            setError(e.message);
        } finally {
            setPending(false);
        }
    };

    return (
        <>
            <Dialog aria-labelledby="import-csv-dialog-title" open={props.open}>
                <DialogTitle id="import-csv-dialog-title">
                    <span className={classes.dialogTitle}>Import CSV</span>
                    <Button onClick={props.onClose} disabled={pending}>
                        Close
                    </Button>
                </DialogTitle>
                <Box className={classes.dialogBody}>
                    <Dropzone onDrop={onDrop} multiple={false} accept="text/csv">
                        {({ getRootProps, getInputProps }) => (
                            <section>
                                <div {...getRootProps()}>
                                    <input {...getInputProps()} />
                                    <Paper className={classes.dropzone} elevation={0}>
                                        <div className={classes.dropzoneInner}>
                                            {!file && !pending && !error && !success && (
                                                <>
                                                    <FileIcon className={classes.dropIcon} />
                                                    <p>Drop an image file here, or click to select files</p>
                                                </>
                                            )}
                                            {pending && (
                                                <>
                                                    <CircularProgress size={40} className={classes.dropIcon} />
                                                </>
                                            )}
                                            {success && (
                                                <>
                                                    <CheckIcon className={`${classes.dropIcon} ${classes.successIcon}`} />
                                                    {success}
                                                </>
                                            )}
                                            {error && (
                                                <>
                                                    <ErrorIcon className={`${classes.dropIcon} ${classes.errorIcon}`} />
                                                    <Box>{error}</Box>
                                                </>
                                            )}
                                            <Box>{fileDetails}</Box>
                                        </div>
                                    </Paper>
                                </div>
                            </section>
                        )}
                    </Dropzone>
                </Box>
            </Dialog>
        </>
    );
};

export default ImportCsvDialog;

const bytes = (size: number) => {
    return size;
};

const useStyles = makeStyles((theme) => ({
    dialogBody: {
        width: 450,
    },
    dialogTitle: {
        marginRight: theme.spacing(2),
    },
    box: {
        padding: '10px',
    },
    dropzone: {
        position: 'relative',
        backgroundColor: theme.palette.grey[200],
        border: `2px dashed ${theme.palette.grey[400]}`,
        color: theme.palette.grey[500],
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        width: '100%',
    },
    dropzoneInner: {
        padding: theme.spacing(4, 2),
        textAlign: 'center',
        height: '100%',
        width: '100%',
    },
    dropIcon: {
        display: 'block',
        margin: '0 auto',
        fontSize: 40,
        marginBottom: theme.spacing(1),
    },
    successIcon: {
        color: green[500],
    },
    errorIcon: {
        color: red[500],
    },
}));

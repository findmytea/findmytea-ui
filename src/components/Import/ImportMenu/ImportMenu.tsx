import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

interface ExportMenuProps {
    onImportTwitter(): void;
    onImportCsv(): void;
    disabled?: boolean;
}

const ExportMenu = (props: ExportMenuProps) => {
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event: any) => {
        setAnchorEl(event.currentTarget);
    };

    const importTwitter = () => {
        handleClose();
        props.onImportTwitter();
    };

    const importCsv = () => {
        handleClose();
        props.onImportCsv();
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} disabled={props.disabled}>
                Import Menu
            </Button>
            <Menu id="simple-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
                <MenuItem onClick={importCsv} disabled={true || props.disabled}>
                    Import from CSV...
                </MenuItem>
                <MenuItem onClick={importTwitter} disabled={props.disabled}>
                    Import from Twitter...
                </MenuItem>
            </Menu>
        </div>
    );
};

export default ExportMenu;

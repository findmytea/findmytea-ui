import Location from './Location';

export default class LocationResponse {
    public locations: Array<Location> = [];
}

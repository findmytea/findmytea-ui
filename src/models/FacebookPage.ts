export default class FacebookName {
    id: number = 0;
    name: string = '';
    link: string = '';
    about: string = '';
    image: string = '';
}

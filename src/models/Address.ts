export class Address {
    public organisation = '';
    public premise = '';
    public street = '';
    public addressLine1 = '';
    public dependentLocality = '';
    public doubleDependentLocality = '';
    public addressLine2 = '';
    public townCity = '';
    public county = '';
    public postcode = '';
}

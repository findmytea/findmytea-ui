import Image from './Image';

export default class Brand {
    id: string = '';
    public name: string = '';
    public description: string = '';
    public website: string = '';
    public image?: Image;
}

export class SocialItem {
    public id: string = '';
    public name: string = '';
    public displayName: string = '';
    public imageUrl?: string = undefined;
    public website?: string = undefined;
    public description?: string = undefined;
}

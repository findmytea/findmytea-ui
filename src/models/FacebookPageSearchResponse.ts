import FacebookPage from './FacebookPage';

export default class FacebookPageSearchResponse {
    public pages: Array<FacebookPage> = [];
}

export class User {
    userName: string = '';
    email: string = '';
    roles: Array<string> = [];
}

class Credentials {
    idToken: string = '';
    bearerToken: string = '';
}

export default class Session {
    user: User = new User();
    credentials: Credentials = new Credentials();
    isLoggedIn: boolean = false;
}

const hasRole = (user: User, name: string) => user.roles.includes(name);

export { hasRole };

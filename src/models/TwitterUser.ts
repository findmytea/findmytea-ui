export default class TwitterUser {
    id: number = 0;
    screenName: string = '';
    name: string = '';
    description: string = '';
    imageUrl: string = '';
    smallImageUrl: string = '';
    website: string = '';
}

import TwitterUser from './TwitterUser';

export default class TwitterUserSearchResponse {
    public users: Array<TwitterUser> = [];
}

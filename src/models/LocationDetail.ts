import Location from './Location';
import Image from './Image';

export default class LocationDetail extends Location {
    public email = '';
    public phone = '';
    public twitterId = 0;
    public twitterHandle = '';
    public facebookId = 0;
    public facebookName = '';
    public facebookLink = '';
    public images: Array<Image> = [];
    public geoX: number = 0.0;
    public geoY: number = 0.0;
}

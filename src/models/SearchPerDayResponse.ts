import { SearchPerDay } from './SearchPerDay';

export default class SearchPerDayResponse {
    public searchesPerDay: Array<SearchPerDay> = [];
}

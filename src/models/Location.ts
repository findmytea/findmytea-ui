import Brand from './Brand';
import Image from './Image';

export default class Location {
    public id = '';
    public name = '';
    public description = '';
    public addressLine1 = '';
    public addressLine2 = '';
    public town = '';
    public county = '';
    public country = '';
    public postcode = '';
    public website = '';
    public distance = 0;
    public image?: Image;
    public retailer = false;
    public cafe = false;
    public brands: Array<Brand> = [];
}

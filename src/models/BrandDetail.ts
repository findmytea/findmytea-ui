import Image from './Image';
import Brand from './Brand';

export default class BrandDetail extends Brand {
    public facebookId = 0;
    public facebookName = '';
    public facebookLink = '';
    public twitterHandle: string = '';
    public twitterId: number = 1;

    public phone = '';
    public email = '';

    public images: Array<Image> = [];
}

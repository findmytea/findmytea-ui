import axios from 'axios';
import Session from '../models/Session';
import { HEADERS } from '../config';

const jwt = require('jwt-decode');

const AUTH = {
    identityBaseUri: process.env.REACT_APP_IDENTITY_BASE_URI ?? '',
    clientUri: process.env.REACT_APP_CLIENT_URI ?? '',
    logoutRedirectUri: process.env.REACT_APP_LOGOUT_REDIRECT_URI ?? '',
    clientKey: process.env.REACT_APP_CLIENT_KEY ?? '',
};

const getLoginUri = () => {
    const callback = encodeURI(`${AUTH.clientUri}/callback`);
    const signinUri = `${AUTH.identityBaseUri}/Token/IdToken?RedirectUrl=${callback}&key=${AUTH.clientKey}`;
    return signinUri;
};

const logout = async () => {
    const callback = encodeURI(AUTH.logoutRedirectUri);
    const signoutUri = `${AUTH.identityBaseUri}/Token/Logout?RedirectUrl=${callback}&key=nothing`;
    window.location.href = signoutUri;
};

const extractTokenFromResponse = (url: string) => {
    const regex = /token=(\{){0,1}[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}(\}){0,1}/g;
    const matches = url.match(regex);
    if (matches?.length !== 1) return undefined;
    return matches[0].substr('token='.length);
};

const parseWebResponse = async (href: string) => {
    const idToken = extractTokenFromResponse(href);
    return await refreshSession(idToken);
};

const refreshSession = async (idToken?: string) => {
    const body = {
        key: AUTH.clientKey,
        idToken: idToken,
    };

    const url2 = `${AUTH.identityBaseUri}/Token`;
    return await axios.post<string>(url2, body, HEADERS);
};

const getSession = (jwtToken: string) => {
    const token = jwt(jwtToken);
    const session = new Session();
    session.user.email = token.sub;
    session.user.userName = token.username;
    session.user.roles = _getRoles(token);
    session.credentials.bearerToken = jwtToken;
    session.credentials.idToken = token.idtoken ?? '';
    session.isLoggedIn = true;
    return session;
};

const _getRoles = (token: any): Array<string> => {
    var roles: Array<string> = [];

    const element = token['fmi:roles'];

    if (element) {
        if (element instanceof Array) {
            element.forEach(function (item: string) {
                roles.push(item);
            });
        } else {
            roles.push(element);
        }
    }

    return roles;
};

export default {
    getLoginUri,
    logout,
    parseWebResponse,
    getSession,
    refreshSession,
};

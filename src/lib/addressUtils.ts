import Location from './../models/Location';

const address = (location: Location) => {
    return [location.addressLine1, location.addressLine2, location.town, location.county, location.postcode].filter((x) => typeof x === 'string' && x.length > 0).join(', ');
};

export { address };

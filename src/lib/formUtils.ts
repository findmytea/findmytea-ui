import { FormProps } from './formProps';

export const setFieldNumber = (props: FormProps, evt: any, name: string) => {
    props.formik.setFieldValue(name, +evt?.target?.value);
};

export const setFieldValue = (props: FormProps, evt: any, name: string) => {
    props.formik.setFieldValue(name, evt?.target?.value);
};

export const setFieldChecked = (props: FormProps, evt: any, name: string) => {
    props.formik.setFieldValue(name, evt?.target?.checked);
};

export const emptyIfNull = (val: string): string => {
    return !val ? '' : val;
};

export const zeroIfNull = (val: number): number => {
    return !val ? 0 : val;
};

export const falseIfNull = (val: boolean): boolean => {
    return !val ? false : val;
};

import get from 'keypather/get';

const keypatherGet = (obj, name) => {
    return get(obj, name);
};

export { keypatherGet as get };

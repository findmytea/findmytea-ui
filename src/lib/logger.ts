import winston from 'winston';
import { uuid } from 'uuidv4';
import Transport from 'winston-transport';
import SumoLogger, { SumoLoggerOptions } from 'sumo-logger';

const sumuEndPoint = process.env.REACT_APP_SUMO_ENDPOINT ? process.env.REACT_APP_SUMO_ENDPOINT : '';

// prod/client/findmytea-ui
class SumoTransport extends Transport {
    private sumoLoggerOptions: SumoLoggerOptions = {
        endpoint: sumuEndPoint,
        sessionKey: uuid(),
        clientUrl: process.env.REACT_APP_AUTH_SIGNOUT_URL,
        onSuccess: () => {
            console.log('Logs sent to Sumo successfully.');
        },
        onError: () => {
            console.error('Sumo log error.');
        },
    };

    private sumoLogger: SumoLogger;

    constructor(opts: any) {
        super(opts);
        this.sumoLogger = new SumoLogger(this.sumoLoggerOptions);
    }

    log(info: any, callback: Function) {
        setImmediate(() => {
            this.emit('logged', info);
        });

        this.sumoLogger.log(JSON.stringify(info));
        this.sumoLogger.flushLogs();
        callback();
    }
}

class FindMyTeaTransport extends Transport {
    log(info: any, callback: Function) {
        setImmediate(() => {
            this.emit('logged', info);
        });

        const date = new Date();
        const lang = 'en-UK';
        console.log(date.toLocaleDateString(lang) + ' - ' + date.toLocaleTimeString(lang) + ' [' + info.level + '] ' + info.message);
        callback();
    }
}

const logger = winston.createLogger({});

const configureLoggerUser = (username?: string) => {
    logger.configure({
        level: process.env.REACT_APP_LOG_LEVEL,
        format: winston.format.json(),
        defaultMeta: { application: 'findmytea', user: username },
    });

    logger.add(new FindMyTeaTransport());

    if (process.env.NODE_ENV === 'production' && sumuEndPoint) {
        logger.add(
            new SumoTransport({
                format: winston.format.simple(),
            })
        );
    }
};

configureLoggerUser(undefined);

export default logger;
export { configureLoggerUser };

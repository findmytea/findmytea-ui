import React from "react";
import HttpsRedirect from "react-https-redirect"

// Material-UI provides a CssBaseline component to kickstart an elegant, consistent, and simple baseline to build upon.
import CssBaseline from "@material-ui/core/CssBaseline";

// Overall App Container
import AppContainer from "./components/AppContainer";

import Routes from "./routes";

import { BrowserRouter as Router } from "react-router-dom";

function App() {
  return (
    <React.Fragment>
      <CssBaseline />
      <HttpsRedirect>
        <Router>
          <AppContainer>
            <Routes />
          </AppContainer>
        </Router>
      </HttpsRedirect>
    </React.Fragment>
  );
}

export default App;

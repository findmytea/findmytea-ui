import axios from 'axios';
import { API } from './../../config';
import queryString from 'query-string';
import TwitterUserSearchResponse from '../../models/TwitterUserSearchResponse';

export async function searchTwitter(searchTerm: string) {
    const query =
        '?' +
        queryString.stringify({
            searchTerm: searchTerm,
        });

    const result = await axios.get<TwitterUserSearchResponse>(API.TWITTER_USER_SEARCH + query);
    return result.data;
}

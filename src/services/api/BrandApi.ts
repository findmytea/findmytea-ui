import axios from 'axios';
import { API, HEADERS } from '../../config';
import BrandResponse from '../../models/BrandResponse';
import BoolResponse from '../../models/BoolResponse';
import BrandDetail from '../../models/BrandDetail';

export async function getBrands() {
    const result = await axios.get<BrandResponse>(API.BRANDS);
    return result?.data;
}

export async function getBrand(id: string) {
    const result = await axios.get<BrandDetail>(API.BRAND.replace(':Id', id));
    return result?.data;
}

export async function deleteBrand(id: string) {
    await axios.delete(API.DELETE_BRAND.replace(':Id', id));
}

export async function importBrandFromTwitter(twitterId: number) {
    const result = await axios.post(API.IMPORT_BRAND_FROM_TWITTER, { twitterId: twitterId }, HEADERS);
    return result?.data;
}

export async function importBrandFromCsv(file: any) {
    const formData = new FormData();
    formData.append('file', file);

    const result = await axios.post<BrandResponse>(API.IMPORT_BRAND_FROM_CSV, formData, {
        headers: {
            'Content-Type': 'multipart/form-data',
        },
    });

    return result?.data;
}

export async function createBrand(brand: BrandDetail) {
    const result = await axios.post(API.CREATE_BRAND, clean(brand), HEADERS);
    return result?.data;
}

export async function updateBrand(brand: BrandDetail) {
    const result = await axios.put(API.UPDATE_BRAND.replace(':Id', brand.id), clean(brand), HEADERS);

    return result?.data;
}

export async function checkName(name: string) {
    const result = await axios.get<BoolResponse>(API.BRAND_NAME_EXISTS.replace(':name', name));
    return result?.data;
}

// export async function exists(twitterId: number) {
//     const result = await axios.get<BoolResponse>(API.TWITTER_ID_EXISTS.replace(':Id', String(twitterId)));
//     return result?.data;
// }

const clean = (brand: BrandDetail) => {
    if (brand.image && !brand.image?.id) delete brand.image;
    return brand;
};

import axios from 'axios';
import { GeoCode } from '../../models/GeoCode';
import { API } from '../../config';

export async function geoCode(address: string) {
    const result = await axios.get<GeoCode>(API.GEO_CODE.replace(':Address', encodeURI(address)));
    return result.data;
}

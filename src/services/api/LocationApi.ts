import axios from 'axios';
import { API, HEADERS } from '../../config';
import LocationResponse from '../../models/LocationResponse';
import LocationDetail from '../../models/LocationDetail';

export async function getLocations() {
    const result = await axios.get<LocationResponse>(API.LOCATIONS);
    return result?.data;
}

export async function getLocation(id: string) {
    const result = await axios.get<LocationDetail>(API.LOCATION.replace(':Id', id));
    return result?.data;
}

export async function createLocation(location: LocationDetail) {
    const result = await axios.post(API.CREATE_LOCATION, clean(location), HEADERS);
    return result?.data;
}

export async function updateLocation(location: LocationDetail) {
    const result = await axios.put(API.UPDATE_LOCATION.replace(':Id', location.id), clean(location), HEADERS);
    return result?.data;
}

export async function deleteLocation(id: string) {
    await axios.delete(API.DELETE_LOCATION.replace(':Id', id));
}

const clean = (location: LocationDetail) => {
    if (location.image && !location.image?.id) delete location.image;
    return location;
};

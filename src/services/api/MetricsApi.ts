import axios from 'axios';
import { API } from '../../config';
import SearchPerDayResponse from '../../models/SearchPerDayResponse';

export async function getSearchesByDay() {
    const result = await axios.get<SearchPerDayResponse>(API.METRICS_SEARCHES_BY_DAY);
    return result?.data;
}

import axios from 'axios';
import { API } from './../../config';
import queryString from 'query-string';
import LocationResponse from '../../models/LocationResponse';

export async function search(searchTerm: string) {
    const query =
        '?' +
        queryString.stringify({
            searchTerm: searchTerm,
            distance: 100000,
        });
    return axios.get<LocationResponse>(API.LOACTION_SEARCH + query);
}

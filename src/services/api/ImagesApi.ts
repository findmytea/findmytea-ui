import axios from 'axios';
import { API } from './../../config';
import Image from '../../models/Image';

export async function loadImages(associateId: string) {
    const url = associateId
        ? API.IMAGES + '?associateId=' + associateId
        : API.IMAGES;
    const result = await axios.get<Array<Image>>(url);
    return result.data;
}

export async function uploadImage(file: any) {
    const formData = new FormData();
    formData.append('file', file);

    const result = await axios.post<Image>(API.IMAGE_UPLOAD, formData, {
        headers: {
            'Content-Type': 'multipart/form-data',
        },
    });

    return result.data;
}

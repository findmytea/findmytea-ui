import axios from 'axios';
import { API } from '../../config';
import queryString from 'query-string';
import FacebookPageSearchResponse from '../../models/FacebookPageSearchResponse';

export async function searchFacebook(searchTerm: string) {
    const query =
        '?' +
        queryString.stringify({
            searchTerm: searchTerm,
        });

    const result = await axios.get<FacebookPageSearchResponse>(API.FACEBOOK_PAGE_SEARCH + query);
    return result.data;
}

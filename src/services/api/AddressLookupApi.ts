import axios from 'axios';
import { API } from './../../config';
import { Address } from '../../models/Address';

export async function lookupAddress(postcode: string) {
    const result = await axios.get<Array<Address>>(API.ADDRESS_LOOKUP.replace(':searchTerm', postcode));
    return result.data;
}
